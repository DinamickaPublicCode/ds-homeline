<?php 
defined( 'ABSPATH' ) || exit;
?>
<?php get_header(); ?>
<section class="layout__sect">
    <div class="container">
    	<?php woocommerce_breadcrumb(); ?>
    	
    	<div class="layout__sect">
        	<?php 
        	if ( have_posts() ) {
        	    
        	    while( have_posts() ) {
        	        
        	        the_post(); 
        	        
        	        the_title('<div class="layout__title"><h2>', '</h2></div>'); 
        	       
        	        ?><div class="layout__sect-text full--content"><?php
        	        the_content();
        	        ?></div><?php
        	        
        	    }
        	    
        	}
        	?>
        	
    	</div>
    </div>
</section>
<?php get_footer();?>