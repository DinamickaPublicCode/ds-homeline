<?php 
defined( 'ABSPATH' ) || exit;
?>
<?php get_header(); ?>
<section class="layout__sect">
	<div class="container">
		<div class="error__banner">
			<div class="error__banner-part is--first" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/box111@4x.png')">
				<h2>Упс...Эта страница пустая:(</h2>
			</div>

			<div class="error__banner-part is--second"style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/box222@4x.png');"></div>
		</div>

		<div class="error__text">
			<p>
				Запрошенная страница не найдена. Возможно она была удалена или <br>
				перемещена по новому адресу. А возможно, ее вообще никогда и не
				было...
			</p>
		</div>

		<div class="error__btns">
			<div class="error__btns-item">
				<a href="#" class="btn">Вернуться назад</a>
			</div>

			<div class="error__btns-item">
				<a href="#" class="btn">На главную</a>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>