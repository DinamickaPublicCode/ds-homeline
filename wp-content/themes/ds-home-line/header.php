<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head>
  <?php wp_head(); ?>
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/favicons/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/favicons/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/favicons/favicon.ico" type="image/x-icon">
</head>

<body <?php body_class(); ?>>
  
    <header class="header">
      <div class="container">
        <div class="header__items">

          <div class="header__item header__logo">
            <a href="/" class="logo">
              <img src="<?php echo get_template_directory_uri();?>/assets/front-end/app/img/logo/logo.svg" alt="logo">
						</a>
						
						<?php /* 
            <a href="/" class="logo">
              <?php 
							$logoID = carbon_get_theme_option('header_logo');
							?>
              <img src="<?php echo wp_get_attachment_image_url($logoID); ?>" alt="logo">
            </a>*/?>
          </div>

          <?php get_template_part('template-parts/common/mini', 'cart'); ?>


          <div class="header__item">
            <a href="#" class="user-icon-wrap" id="profile">
              <svg class="user-icon">
                <use class="user-icon__part"
                  xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#user">
                </use>
              </svg>
            </a>
            <?php if ( is_user_logged_in() ) { ?>
            <?php $user = wp_get_current_user(); ?>
            <div class="profile-popin" id="profile-blank">
              <div class="profile-popin__row">
                <div class="profile-popin__img">
                  <img src="<?php echo get_template_directory_uri();?>/assets/front-end/app/img/no-image.png"
                    alt="Юлия Анисимова" class="object-fit is--cover" data-object-fit="cover">
                </div>
                <div class="profile-popin__name">
                  <p><?php echo $user->first_name; ?>&nbsp;<?php echo $user->last_name; ?></p>
                </div>
              </div>
              <div class="profile-popin__links">
                <a href="/my-account/" class="profile-popin__link"><span>Мой профиль</span></a>
                <a href="<?php echo wc_logout_url(); ?>" class="profile-popin__link">
                  <span>Выход</span>
                  <div class="profile-popin__icon">
                    <svg class="logout-icon">
                      <use class="logout-icon__part" xlink:href="img/icons/sprite.svg#logout"></use>
                    </svg>
                  </div>
                </a>
              </div>
            </div>
            <?php } else { ?>
            <div class="profile-popin" id="profile-blank">
              <div class="profile-popin__links">
                <a href="/my-account/" class="profile-popin__link"><span>Вход / Регистрация</span></a>
              </div>
            </div>
            <?php } ?>
          </div>


          <div class="header__item">
            <div class="sandwich" id="sandwich">
              <span class="sandwich__part"></span>
              <span class="sandwich__part"></span>
              <span class="sandwich__part"></span>
            </div>
          </div>

        </div>

        <div class="header__nav" id="navigation">

          <div class="header__nav-top">
            <?php /* <a href="/" class="header__nav-logo">
              <?php $menuLogo = carbon_get_theme_option('menu_logo'); ?>
              <img src="<?php echo wp_get_attachment_image_url($menuLogo); ?>" alt="DS">
						</a>*/?>
						
						<a href="/" class="header__nav-logo">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/logo/elephant_1.svg" alt="DS">
            </a>
          </div>


          <?php 
					$menu = getMenuTree('main_menu');
					?>
          <nav class="nav" role="navigation">
            <ul class="nav__list">
              <?php if (!empty($menu)) { ?>
              <?php foreach ($menu as $item) { ?>
              <?php if (!isset($item['sub_menu'])) { ?>
              <li class="nav__item">
                <a href="<?php echo $item['url']; ?>" class="nav__link"><?php echo $item['title']?></a>
              </li>
              <?php } else { ?>
              <li class="nav__item">
                <a href="<?php echo $itemp['url']; ?>" class="nav__link" data-menu="sub-menu">
                  <span class="is--submenu">
                    <span><?php echo $item['title']?></span>
                    <span class="submenu-icon right-arrow-wrap">
                      <svg class="arrow-icon">
                        <use class="arrow-icon__part is--dark"
                          xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#right-arrow">
                        </use>
                      </svg>
                    </span>
                  </span>
                </a>
                <ul class="nav__list sub--menu">
                  <?php foreach ($item['sub_menu'] as $sub) { ?>
                  <li class="nav__item is--submenu--second">
                    <!-- menu item -->
                    <a href="<?php echo $sub['url']; ?>" class="nav__link"><?php echo $sub['title']; ?></a>
                    <?php 
                									$prodIDs = carbon_get_nav_menu_item_meta($sub['ID'], 'menu_products');
                									if (!empty($prodIDs)) {
                									?>
                    <ul class="nav__list sub--menu--second">
                      <!-- menu item products -->
                      <?php 
                										$_pf = new WC_Product_Factory();
                										foreach ($prodIDs as $pid) {
                										    $_product = $_pf->get_product($pid['id']);
                										?>
                      <li class="nav__item">
                        <div class="product-card">
                          <a href="<?php echo $_product->get_permalink(); ?>" class="product-card__img">
                            <img src="<?php echo wp_get_attachment_image_url($_product->get_image_id(), 'large'); ?>"
                              alt="card">
                          </a>
                          <div class="product-card__info">
                            <p><strong><?php echo $_product->get_title(); ?></strong></p>
                            <p><?php echo $_product->get_price();?> грн</p>
                            <div class="product-card__detail">
                              <a href="<?php echo $_product->get_permalink(); ?>" class="btn-link"> <span>Перейти</span>
                                <div class="btn-link__icon arrow-icon-wrap is--next">
                                  <svg class="arrow__icon">
                                    <use class="arrow__part"
                                      xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#left-arrow">
                                    </use>
                                  </svg>
                                </div>
                              </a>
                            </div>
                          </div>
                        </div>
                      </li>
                      <?php } ?>
                    </ul> <!-- end menu item products -->
                    <?php } ?>
                  </li><!-- end menu item  -->
                  <?php } ?>
                </ul>
              </li>
              <?php } ?>
              <?php } ?>
              <?php } ?>
            </ul>
          </nav>

          <div class="header__nav-bottom">
            <div class="socials socials--secondary">
              <?php 
							$menuSocial = carbon_get_theme_option('menu_social');
							if (!empty($menuSocial)) {
							?>
              <ul class="socials__list">
                <?php foreach ($menuSocial as $social) { ?>
                <?php if ($social['type'] == 'inst') { ?>
                <li class="socials__item">
                  <a href="<?php echo $social['href']; ?>" class="socials__link">
                    <img
                      src="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/socials/instagram-dark.svg"
                      alt="facebook">
                  </a>
                </li>
                <?php } else if ($social['type'] == 'phone') { ?>
                <li class="socials__item">
                  <a href="<?php echo $social['href']; ?>" class="socials__link">
                    <svg class="phone-icon">
                      <use class="phone-icon__part is--dark"
                        xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#telephone">
                      </use>
                    </svg>
                  </a>
                </li>
                <?php } else if ($social['type'] == 'env') { ?>
                <li class="socials__item">
                  <a href="<?php echo $social['href']; ?>" class="socials__link">
                    <svg class="email-icon">
                      <use class="email-icon__part is--dark"
                        xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#envelope">
                      </use>
                    </svg>
                  </a>
                </li>
                <?php } else if ($social['type'] == 'fb') { ?>
                <li class="socials__item">
                  <a href="<?php echo $social['href']; ?>" class="socials__link">
                    <img
                      src="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/socials/facebook-dark.svg"
                      alt="facebook">
                  </a>
                </li>
                <?php } ?>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
          </div>

        </div>
      </div>
    </header>
<div class="wrapper">
    <main class="layout" role="main">