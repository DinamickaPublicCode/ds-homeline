<?php 
/**
 * Core functions file.
 * Define all necessary dependencies
 * @link https://codex.wordpress.org/Functions_File_Explained
 */
//Additional theme functions
require get_template_directory() . '/includes/functions.php';
//Theme settings configuration class instance
require get_template_directory() . '/includes/classes/ThemeSettings.php';
//Additional woocommerce theme functions
require get_template_directory() . '/includes/wc-functions.php';
//Theme custom post_types configuration class instance
require get_template_directory() . '/includes/classes/PostTypes.php';
//Theme custom scripts main class
require get_template_directory() . '/includes/classes/ThemeScripts.php';
//Theme custom fields main class
require get_template_directory() . '/includes/classes/ThemeCustomFields.php';
//Woocommerce custom fields
require get_template_directory() . '/includes/classes/WooCustomFields.php';
