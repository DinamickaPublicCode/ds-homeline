        </main>
      
        <footer class="footer">
          <div class="footer__top">
            <div class="container">
              <div class="footer__items">

                <div class="footer__item">
                  <div class="email-icon-wrap">
                    <svg class="email-icon">
                      <use class="email-icon__part"
                        xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#envelope">
                      </use>
                    </svg>
                  </div>
                  <div class="emails">
                    <?php $footerMail = carbon_get_theme_option('footer_email'); ?>
                    <a href="mailto:<?php echo $footerMail; ?>"><?php echo $footerMail; ?></a>
                  </div>
                </div>

                <div class="footer__item">
                  <div class="phone-icon-wrap">
                    <svg class="phone-icon">
                      <use class="phone-icon__part"
                        xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#telephone">
                      </use>
                    </svg>
                  </div>
                  <div class="phones">
                    <?php $footerPhone = carbon_get_theme_option('footer_phone'); ?>
                    <a href="tel:<?php echo $footerPhone; ?>"><?php echo $footerPhone; ?></a>
                  </div>
                </div>

                <div class="footer__item socials">
                  <?php 
							$menuSocial = carbon_get_theme_option('footer_social');
							if (!empty($menuSocial)) {
							?>
                  <ul class="socials__list">
                    <?php foreach ($menuSocial as $social) { ?>
                    <?php if ($social['type'] == 'inst') { ?>
                    <li class="socials__item">
                      <a href="<?php echo $social['href']; ?>" class="social__link" rel="nofollow" target="_blank">
                        <img
                          src="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/socials/instagram.svg"
                          alt="facebook" width="30" height="30">
                      </a>
                    </li>
                    <?php } else if ($social['type'] == 'fb') { ?>
                    <li class="socials__item">
                      <a href="<?php echo $social['href']; ?>" class="social__link" rel="nofollow" target="_blank">
                        <img
                          src="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/socials/facebook.svg"
                          alt="instagram" width="30" height="30">
                      </a>
                    </li>
                    <?php } ?>
                    <?php } ?>
                  </ul>
                  <?php } ?>
                </div>

              </div>
            </div>
          </div>
          <div class="container">
            <div class="footer__bottom">
              <div class="footer-nav">
                <?php $footer1 = getMenuTree('footer_1'); ?>
                <ul class="footer-list">
                  <?php if (!empty($footer1)) { ?>
                  <?php foreach ($footer1 as $item) { ?>
                  <li class="footer-list__item">
                    <a href="<?php echo $item['url']; ?>"><?php echo $item['title']?></a>
                  </li>
                  <?php } ?>
                  <?php } ?>
                </ul>
                <?php $footer2 = getMenuTree('footer_2'); ?>
                <ul class="footer-list">
                  <?php if (!empty($footer2)) { ?>
                  <?php foreach ($footer2 as $item) { ?>
                  <li class="footer-list__item">
                    <a href="<?php echo $item['url']; ?>"><?php echo $item['title']?></a>
                  </li>
                  <?php } ?>
                  <?php } ?>
                </ul>
                <?php $footer3 = getMenuTree('footer_3'); ?>
                <ul class="footer-list">
                  <?php if (!empty($footer3)) { ?>
                  <?php foreach ($footer3 as $item) { ?>
                  <li class="footer-list__item">
                    <a href="<?php echo $item['url']; ?>"><?php echo $item['title']?></a>
                  </li>
                  <?php } ?>
                  <?php } ?>
                </ul>
                <?php $footer4 = getMenuTree('footer_4'); ?>
                <ul class="footer-list">
                  <?php if (!empty($footer4)) { ?>
                  <?php foreach ($footer4 as $item) { ?>
                  <li class="footer-list__item">
                    <a href="<?php echo $item['url']; ?>"><?php echo $item['title']?></a>
                  </li>
                  <?php } ?>
                  <?php } ?>
                </ul>
              </div>
              <div class="copyright">
                <p><a href="https://perfectorium.com/">Powered by Perfectorium</a></p>
                <p>All rights reservered</p>
              </div>
            </div>
          </div>
        </footer>

        <div class="pop-ups">

          <div class="pop__up" data-name-popup="cart-tbl">
            <?php woocommerce_mini_cart(); ?>
          </div>

          <div class="pop__up" data-name-popup="form-order">
            <div class="pop__up-row">
              <?php echo do_shortcode(carbon_get_theme_option('bof_form_code')); ?>
            </div>
          </div>

          <div class="pop__up" data-name-popup="success-order">
            <div class="layout-alert pop__up-row">
              <div class="pop__up-blank">
                <div class="layout-alert__img">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/logo/alertEl.svg"
                    alt="DS HOME LINE">
                </div>

                <div class="layout-alert__title">
                  <h3>Благодарим за заказ:)</h3>
                </div>

                <div class="layout-alert__subtitle">
                  <p>В скором времени мы свяжемся с Вами</p>
                </div>

                <div class="close" data-name-popup="success-order">
                  <span class="close__part"></span>
                  <span class="close__part"></span>
                </div>
              </div>
            </div>
          </div>

          <div class="pop__up" data-name-popup="success-order2">
            <div class="layout-alert pop__up-row">
              <div class="pop__up-blank">
                <div class="layout-alert__img">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/logo/alertEl.svg" alt="DS HOME LINE">
                </div>

                <div class="layout-alert__title">
                  <h3>Ожидайте звонка</h3>
                </div>

                <div class="layout-alert__subtitle">
                  <p>В скором времени мы свяжемся с Вами</p>
                </div>

                <div class="close" data-name-popup="success-order2">
                  <span class="close__part"></span>
                  <span class="close__part"></span>
                </div>
              </div>
            </div>
          </div>
        </div>

        </div>

				<a href="#" class="succes-order-btn2" data-get-popup="success-order" type="hidden"></a>						
        <a href="#" class="succes-order-btn" data-get-popup="success-order2" type="hidden"></a>
        <?php wp_footer(); ?>

        <script>
						$(".layout__sect .wpcf7-form .wpcf7-submit").on("click", function(e) {
							$(this).parents(".wpcf7-form").addClass("validating");

							setTimeout(() => {
								if ( $(this).parents(".wpcf7-form").hasClass("invalid") ) {
									$(this).parents(".wpcf7-form").removeClass("validating");
								} else {
									$(document).find(".succes-order-btn").click();
									$(this).parents(".wpcf7-form").removeClass("validating");
								}
							}, 1000);
						})

						$(".pop__up-row .wpcf7-form .wpcf7-submit").on("click", function(e) {
							$(this).parents(".wpcf7-form").addClass("validating");

							setTimeout(() => {
								if ( $(this).parents(".wpcf7-form").hasClass("invalid") ) {
									$(this).parents(".wpcf7-form").removeClass("validating");
								} else {
									var top = $("body").css("top");
									$(document).find(".succes-order-btn2").click();
									$("body").css("top", top);
									$(".pop__up[data-name-popup='form-order']").removeClass("is--show");
									$(this).parents(".wpcf7-form").removeClass("validating");
								}	
							}, 1000);
						})
        </script>
        <!-- Facebook Pixel Code -->
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '507055873544285');
          fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
          src="https://www.facebook.com/tr?id=507055873544285&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
<!-- BEGIN PLERDY CODE -->
<script type="text/javascript" defer>
    var _protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
    var _site_hash_code = "fd1cd6e8d7afefe145d23cd6668d6272";
    var _suid = 9593;
    </script>
<script type="text/javascript" defer src="https://a.plerdy.com/public/js/click/main.js"></script>
<!-- END PLERDY CODE -->
        </body>

        </html>