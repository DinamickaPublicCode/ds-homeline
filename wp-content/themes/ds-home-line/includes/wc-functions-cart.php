<?php
/**
 * @package DsHomeLine
 * @version 1.0
 */
defined( 'ABSPATH' ) || exit;

/**
 * Sample implementation of the WooCommerce Mini Cart.
 *
 * You can add the WooCommerce Mini Cart to header.php like so ...
 *
 *  <?php
 *  if ( function_exists( 'dhl_woocommerce_header_cart' ) ) {
 *  dhl_woocommerce_header_cart();
 *  }
 *  ?>
 */

if ( !function_exists('dhl_woocommerce_cart_link_fragment') ) {
    /**
     * Cart Fragments.
     *
     * Ensure cart contents update when products are added to the cart via AJAX.
     *
     * @param array $fragments Fragments to refresh via AJAX.
     * @return array Fragments to refresh via AJAX.
     */
    function dhl_woocommerce_cart_link_fragment( $fragments ) {
        ob_start();
        dhl_woocommerce_cart_icon();
        $fragments['div.mini-cart-ajax'] = ob_get_clean();
        
        ob_start();
        dhl_woocommerce_mini_cart();
        $fragments['div.mini-cart-data'] = ob_get_clean();

        ob_start();
        dhl_woocommerce_full_cart();
        $fragments['section.full-cart'] = ob_get_clean();
        return $fragments;
    }
}
add_filter('woocommerce_add_to_cart_fragments', 'dhl_woocommerce_cart_link_fragment');


if ( !function_exists('dhl_woocommerce_cart_icon') ) {
    /**
     * Cart Link.
     *
     * Displayed a link to the cart including the number of items present and the cart total.
     *
     * @return void
     */
    function dhl_woocommerce_cart_icon() {
        ?>
        <div class="mini-cart-ajax">
    	 	<div class="cart__circle">
            	<span class="cart__value"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
            </div>
            <div class="cart-icon-wrap">
                <svg class="cart-icon">
    				<use class="cart-icon__part" xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#cart"></use>
                </svg>
            </div>
        </div>
		<?php
	}
} 

if ( !function_exists('dhl_woocommerce_mini_cart') ) {
	/**
	 * Display Header Cart.
	 *
	 * @return void
	 */
    function dhl_woocommerce_mini_cart( $args = array() ) {
	    $defaults = array(
	        
	    ); 
	    $args = wp_parse_args( $args, $defaults );
	    wc_get_template( 'cart/mini-cart.php', $args );
	}
}

if ( !function_exists('dhl_woocommerce_full_cart') ) {
	/**
	 * Display Full Cart.
	 *
	 * @return void
	 */
    function dhl_woocommerce_full_cart( $args = array() ) {
	    $defaults = array(
	        
	    ); 
        $args = wp_parse_args( $args, $defaults );
        if (WC()->cart->is_empty()) {
            wc_get_template( 'cart/cart-empty.php', $args );
        } else {
            wc_get_template( 'cart/cart.php', $args );
        }
	   
	}
}