<?php
/**
 * Custom woocommerce add to cart actions
 * @package DsHomeLine
 * @since 1.0
 * @version 1.0
 */
defined( 'ABSPATH' ) || exit;

function dhl_get_variations_list($args = array()) : string {
    
    $html = '';
    
    $attr = $args['attribute'];
    $slug = sanitize_title( $attr );
    $options = $args['options'];
    $product = $args['product'];
    
    if ( !empty($options) && taxonomy_exists( $attr ) ) {
        $terms = wc_get_product_terms($product->get_id(), $attr, array('fields' => 'all'));
        ob_start();
    ?>
        <div class="info-card__row">
            <div class="info-card__title">
            	<h4><?php echo wc_attribute_label( $attr ); // WPCS: XSS ok. ?></h4>
            </div>
            <ul id="<?php echo $slug; ?>" data-type="options-list" class="info-card__list">
                <?php foreach ( $terms as $term ) { ?>
                	<?php if ( in_array( $term->slug, $options, true ) ) { ?>
                        <li class="info-card__item">
                        	<input type="radio" 
                        	value="<?php echo esc_attr( $term->slug ); ?>" 
                        	name="<?php echo $slug; ?>" 
                        	id="<?php echo esc_attr( $term->slug ); ?>">
                        	<label class="btn btn--third" for="<?php echo esc_attr( $term->slug ); ?>"><?php echo $term->name; ?></label>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
    <?php
        $html = ob_get_clean();
    }
    
    return $html;
}

// Custom add-to-cart ajax event
add_action('wp_ajax_dhl_ajax_add_to_cart', 'dhl_ajax_add_to_cart');
add_action('wp_ajax_nopriv_dhl_ajax_add_to_cart', 'dhl_ajax_add_to_cart');
function dhl_ajax_add_to_cart() {
    
    $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
    $quantity = empty($_POST['quantity']) ? 1 : $_POST['quantity'];
    $variation_id = absint($_POST['variation_id']);
    $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
    $product_status = get_post_status($product_id);
    
    if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id) && 'publish' === $product_status) {
        
        $data = 
        do_action('woocommerce_ajax_added_to_cart', $product_id);
        
        if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
            wc_add_to_cart_message(array($product_id => $quantity), true);
        }
        
//         WC_AJAX :: get_refreshed_fragments();
        wp_send_json(WC_AJAX :: get_refreshed_fragments());
    } else {
        
        $data = array(
            'error' => true,
            'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id),
        );
        
        echo wp_send_json($data);
    }
    
    wp_die();
}