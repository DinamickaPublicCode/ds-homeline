<?php
/**
 * Custom|Additional theme functions
 * @package DsHomeLine
 * @since 1.0
 * @version 1.0
 */
if (! defined('ABSPATH')) {
    exit();
}

/**
 * Dumping function
 *
 * @param string|array|object $a
 */
function render($a)
{
    echo '<pre>';
    print_r($a);
    echo '</pre>';
}

/**
 * Get SVG icon by provided type
 *
 * @param string $type
 *            - {gift|diamond|gift-box}
 * @return string
 */
function getAdvantageIcon(string $type): string
{
    $str = '<svg class="biography-icon">';
    $str .= '<use class="biography-icon__part" xlink:href="' . get_template_directory_uri() . '/assets/front-end/app/img/icons/sprite.svg#' . $type . '"></use>';
    $str .= '</svg>';
    return $str;
}

/**
 * Execute id's array from association array
 *
 * @param array $association
 * @return array
 */
function getIDsFromAssociation(array $association): array
{
    return array_map(function ($el) {
        return $el['id'];
    }, $association);
}

/**
 *
 * @param string $location
 * @return boolean|array[]
 */
function getMenuTree($location = '')
{
    $locations = get_nav_menu_locations();

    if (isset($locations[$location])) {
        $menu = wp_get_nav_menu_object($locations[$location]);

        if ($menu && $menu->term_id) {
            $menuID = $menu->term_id;
        }
    }

    $tree = [];
    $new_menu = [];
    if (!empty($menuID)) {
        $menu = wp_get_nav_menu_items($menuID);

        if (empty($menu))
            return false;

        foreach ($menu as $key => $value) {
            $new_menu[$value->ID] = (array) $value;
        }

        foreach ($new_menu as $id => &$node) {
            if (! $node["menu_item_parent"]) {
                $tree[$node["ID"]] = &$node;
            } else {
                $new_menu[$node["menu_item_parent"]]["sub_menu"][$node["ID"]] = &$node;
            }
        }

        unset($node, $new_menu, $menu);
    }
    return $tree;
}

function get_page_url($template_name)
{
    $pages = get_posts([
        'post_type' => 'page',
        'post_status' => 'publish',
        'meta_query' => [
            [
                'key' => '_wp_page_template',
                'value' => 'templates/'.$template_name.'.php',
                'compare' => '='
            ]
        ]
    ]);
    if(!empty($pages))
    {
        foreach($pages as $pages__value)
        {
            return get_permalink($pages__value->ID);
        }
    }
    return get_bloginfo('url');
}



