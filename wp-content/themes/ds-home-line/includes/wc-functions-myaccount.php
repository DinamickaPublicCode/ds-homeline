<?php 
/**
 * Actions|Filters for My-Account page
 * 
 * @package DsHomeLine
 * @since 1.0
 * @version 1.0
 */
defined( 'ABSPATH' ) || exit;

// Validating first_name | last_name fields
add_action( 'woocommerce_register_post', 'dhl_validate_extra_register_fields', 10, 3 );
function dhl_validate_extra_register_fields( $username, $email, $validation_errors ) {
    if ( isset( $_POST['first_name'] ) && empty( $_POST['first_name'] ) ) {
        $validation_errors->add('first_name_error', __('Введите имя пользователя!', 'dhl'));
    }
    if ( isset( $_POST['last_name'] ) && empty( $_POST['last_name'] ) ) {
        $validation_errors->add('last_name_error', __('Введите фамилию пользователя!', 'dhl'));
    }
    return $validation_errors;
}

// Saving custom first_name | last_name fields 
add_action( 'woocommerce_created_customer', 'dhl_save_extra_register_fields' );
function dhl_save_extra_register_fields( $customer_id ) {
    
    if ( isset( $_POST['first_name'] ) ) {
        //First name field which is by default
        update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['first_name'] ) );
        // First name field which is used in WooCommerce
        update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['first_name'] ) );
    }
    if ( isset( $_POST['last_name'] ) ) {
        // Last name field which is by default
        update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['last_name'] ) );
        // Last name field which is used in WooCommerce
        update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['last_name'] ) );
    }

    if ( isset($_POST['billing_phone']) ) {
        update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
    }
    
}

// Change username same as email address
add_filter( 'woocommerce_new_customer_data', 'generate_username_from_email', 10, 1 );
function generate_username_from_email( $customer_data ){
    $customer_data['user_login'] = sanitize_user( str_replace( ' ', '-', $_POST['email'] ) );
    return $customer_data;
}


add_filter( 'woocommerce_account_menu_items', 'remove_downloads_link' );
function remove_downloads_link( $items ) {
    /**
     * @param array $items [
     *      [dashboard] => Console
     *      [orders] => Orders
     *      [edit-address] => Address
     *      [edit-account] => Details
     *      [customer-logout] => Logout
     *      [downloads] => Downloads
     * ]
     */
    if ( isset( $items['downloads'] ) ) {
        unset( $items['downloads'] );
    }
    if ( isset($items['customer-logout']) ) {
        unset( $items['customer-logout'] );
    }
    if ( isset($items['edit-address']) ) {
        unset( $items['edit-address'] );
    }
    if ( isset($items['edit-account']) ) {
        unset( $items['edit-account'] );
    }
    if ( isset( $items['dashboard'] ) ) {
        $items['dashboard'] = 'Мой профиль';
    }

    if ( isset( $items['orders'] ) ) {
        $items['orders'] = 'Мои заказы';
    }
    
    return $items;
}


// Check and validate the mobile phone
add_action( 'woocommerce_save_account_details_errors','billing_mobile_phone_field_validation', 20, 1 );
function billing_mobile_phone_field_validation( $args ){
    if ( isset($_POST['billing_phone']) && empty($_POST['billing_phone']) ) {
        $args->add( 'error', __( 'Заполните телефон', 'woocommerce' ),'');
    } else if (isset($_POST['billing_phone']) && strlen($_POST['billing_phone']) < 19) {
        $args->add( 'error', __( 'Некорректный телефон', 'woocommerce' ),'');
    }
} 
// Save the mobile phone value to user data
add_action( 'woocommerce_save_account_details', 'my_account_saving_billing_mobile_phone', 20, 1 );
function my_account_saving_billing_mobile_phone( $user_id ) {
    if( isset($_POST['billing_phone']) && ! empty($_POST['billing_phone']) ) {
        update_user_meta( $user_id, 'billing_phone', $_POST['billing_phone']);
    }  
}