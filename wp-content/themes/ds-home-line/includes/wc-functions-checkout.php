<?php
/**
 * @package DsHomeLine
 * @version 1.0
 */
defined( 'ABSPATH' ) || exit;

remove_action( 'woocommerce_before_checkout_form','woocommerce_checkout_coupon_form', 10 );
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );


/**
 * Custom checkout registration form
 */
add_action('wp_ajax_dhl_checkout_registration', 'checkoutRegistration');
add_action('wp_ajax_nopriv_dhl_checkout_registration', 'checkoutRegistration');
function checkoutRegistration() {
    $info = $_POST['data'];
    
    $subscribe = $info['subscribe'] ?? false;
    $info['password'] =  wp_generate_password();
    
    $user = wp_create_user( $info['email'],  $info['password'], $info['email'] );
    
    if ( is_wp_error( $user ) ) {
        wp_send_json_error( array( 'message'    => 'Registration Error',
            'submessage' => $user->get_error_message()
        ) );
    }
   
    update_user_meta( $user, 'first_name', $info['first_name'] );
    update_user_meta( $user, 'shipping_first_name', $info['first_name'] );
    update_user_meta( $user, 'last_name', $info['last_name'] );
    update_user_meta( $user, 'shipping_last_name', $info['last_name'] );
    update_user_meta( $user, 'billing_phone', $info['billing_phone'] );
    
    $new_user = new WP_User( $user );
    $new_user->data->display_name = $info['first_name'] . ' ' . $info['last_name'];
    $new_user->set_role('customer');
    wp_update_user( $new_user );
    
    $signOn = wp_signon(array(
        'user_login'    => $info['email'],
        'user_password' => $info['password'],
    ));
    if ( is_wp_error($signOn) ) {
        wp_send_json_error( array( 
            'message'    => 'Sign On Error',
            'submessage' => $signOn->get_error_message()
        ) );
    }
    
    wp_send_json_success( array(
        'message'    => 'You successfully registered!',
    ) );
    
    wp_die();
}

/**
 * Custom checkout sign-in
 */
add_action('wp_ajax_dhl_checkout_login', 'checkoutLogin');
add_action('wp_ajax_nopriv_dhl_checkout_login', 'checkoutLogin');
function checkoutLogin() {
    $info = $_POST['data'];
    
    $signOn = wp_signon(array(
        'user_login'    => $info['email'],
        'user_password' => $info['password'],
    ));
    if ( is_wp_error($signOn) ) {
        wp_send_json_error( array( 
            'message'    => 'Sign On Error',
            'submessage' => $signOn->get_error_message()
        ) );
    }
    
    wp_send_json_success( array(
        'message'    => 'You successfully logined!',
    ) );
    
    wp_die();
}



/**
 * Get shipping methods.
 */
function dhl_cart_totals_shipping_html() {
    $packages           = WC()->shipping()->get_packages();
    $first              = true;
    
    foreach ( $packages as $i => $package ) {
        $chosen_method = isset( WC()->session->chosen_shipping_methods[ $i ] ) ? WC()->session->chosen_shipping_methods[ $i ] : '';
        $product_names = array();
        
        if ( count( $packages ) > 1 ) {
            foreach ( $package['contents'] as $item_id => $values ) {
                $product_names[ $item_id ] = $values['data']->get_name() . ' &times;' . $values['quantity'];
            }
            $product_names = apply_filters( 'woocommerce_shipping_package_details_array', $product_names, $package );
        }
        
        wc_get_template(
            'checkout/shipping-method.php',
            array(
                'package'                  => $package,
                'available_methods'        => $package['rates'],
                'show_package_details'     => count( $packages ) > 1,
                'show_shipping_calculator' => is_cart() && $first,
                'package_details'          => implode( ', ', $product_names ),
                /* translators: %d: shipping package number */
                'package_name'             => apply_filters( 'woocommerce_shipping_package_name', ( ( $i + 1 ) > 1 ) ? sprintf( _x( 'Shipping %d', 'shipping packages', 'woocommerce' ), ( $i + 1 ) ) : _x( 'Shipping', 'shipping packages', 'woocommerce' ), $i, $package ),
                'index'                    => $i,
                'chosen_method'            => $chosen_method,
                'formatted_destination'    => WC()->countries->get_formatted_address( $package['destination'], ', ' ),
                'has_calculated_shipping'  => WC()->customer->has_calculated_shipping(),
            )
        );
        
        $first = false;
    }
}

if ( ! function_exists( 'dhl_woocommerce_checkout_payment' ) ) {
    
    /**
     * Output the Payment Methods on the checkout.
     */
    function dhl_woocommerce_checkout_payment() {
        if ( WC()->cart->needs_payment() ) {
            $available_gateways = WC()->payment_gateways()->get_available_payment_gateways();
            WC()->payment_gateways()->set_current_gateway( $available_gateways );
        } else {
            $available_gateways = array();
        }
        
        wc_get_template(
            'checkout/payment.php',
            array(
                'checkout'           => WC()->checkout(),
                'available_gateways' => $available_gateways,
                'order_button_text'  => apply_filters( 'woocommerce_order_button_text', __( 'Place order', 'woocommerce' ) ),
            )
        );
    }
}


add_action('wp_ajax_dhl_create_order', 'createOrder');
add_action('wp_ajax_nopriv_dhl_create_order', 'createOrder');
function createOrder() {
    $data = $_POST['data'];
    
    switch($data['shipping_company']) {
        case 'nova' : 
            $data['shipping_company'] = "Нова пошта";
            $data['shipping_address_1'] = "Отделение :" . $data['shipping_department'];
            $data['shipping_address_2'] = $data['shipping_rate_method']['shipping_name'];
            break;
        case 'ukr' : 
            $data['shipping_company'] = "Укр пошта";
            $data['shipping_address_2'] = $data['shipping_rate_method']['shipping_name'];
            break;
    }
    
    $data['billing_first_name'] = $data['shipping_first_name'];
    $data['billing_last_name'] = $data['shipping_last_name'];
    $data['billing_city'] = $data['shipping_city'];
    
    try {

        $checkout = new WC_Checkout();
        $order_id = $checkout->create_order($data);
        if ($order_id > 0) {
            WC()->cart->empty_cart();
            // Get the WC_Email_New_Order object
            $wcMailer = WC()->mailer()->get_emails();
            // Sending the new Order email notification for an $order_id (order ID)
            $wcMailer['WC_Email_New_Order']->trigger( $order_id );
            $wcMailer['WC_Email_Customer_Invoice']->trigger( $order_id );
        }
        wp_send_json_success(array(
            'messsage' => 'Order created',
            'order_id' => $order_id,
            'href' => get_page_url('thankyou')
        ));
        
    } catch ( Exception $e ) {
        wp_send_json_error(array(
            'message' => new WP_Error( 'checkout-error', $e->getMessage() ),
        ));
    }
    
    wp_die();
}

