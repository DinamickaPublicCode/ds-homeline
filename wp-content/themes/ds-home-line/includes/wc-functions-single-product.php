<?php
/**
 * Actions|Filters for single-product
 * @package DsHomeLine
 * @since 1.0
 * @version 1.0
 */
defined( 'ABSPATH' ) || exit;

// Remove woocommerce_output_content_wrapper hook
remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);

// Remove standart breadcrumbs position
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);

// Remove woocommerce_output_content_wrapper_end hook
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

// Remove woocommerce_template_single_excerpt hook
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);

// Remove woocommerce_template_single_meta hook
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

// Remove woocommerce_template_single_sharing hook
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);

// Remove woocommerce_single_variation hook
remove_action('woocommerce_single_variation', 'woocommerce_single_variation', 10);

// Remove woocommerce_output_product_data_tabs hook
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);

// Remove woocommerce_upsell_display hook
remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);

// Remove woocommerce_output_related_products hook
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);


/**
 * Get single product image urls by given ids
 * 
 * @param array $ids
 * @return array
 */
function getProductSimpleGalleryUrls(array $ids) : array {
    return array_map(function ($id) {
        return wp_get_attachment_image_url($id, 'large');
    }, $ids);
}

/**
 * 
 * @param array $variations
 * @return array
 */
function getProductVariationGalleryUrls(array $variations) : array {
    
    $tmp = array();
    foreach ($variations as $variation) {
        if (empty($variation['image_id']) || isset($tmp[$variation['image_id']])) {
            continue;
        }
        // Key will be our img id, to understand in javascript what image we need to trigger for currently
        // clicked product variation
        $tmp[$variation['image_id']] = array(
            'variation_id' => $variation['variation_id'],
            'url' => wp_get_attachment_image_url($variation['image_id'], 'large')
        );
    }
    
   return $tmp;
}


// Single product RATING|PRICE wrapper START
add_action('woocommerce_single_product_summary', 'dhl_rating_price_wrap_start', 7);
function dhl_rating_price_wrap_start() {
    ?><div class="info-card__items"><?php
}
// Single product RATING|PRICE wrapper END
add_action('woocommerce_single_product_summary', 'dhl_rating_price_wrap_end', 14);
function dhl_rating_price_wrap_end() {
    ?></div><?php
}

// Custom single page description layout
add_action('woocommerce_after_single_product', 'dhl_single_product_description', 10);
function dhl_single_product_description(){
    wc_get_template_part('single-product/description');
}



