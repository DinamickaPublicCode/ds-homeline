<?php
/**
 * Woocommerce custom actions|hooks|filters declaration
 * Each require files contains modifications for specified template types
 * @package DsHomeLine
 * @since 1.0
 * @version 1.0
 */
defined( 'ABSPATH' ) || exit;

define( 'WC_MAX_LINKED_VARIATIONS', 100 );

// Remove woocommerce actions|filters
require get_template_directory() . '/includes/wc-functions-remove.php';
// Actions|Filters for single product page
require get_template_directory() . '/includes/wc-functions-single-product.php';
// AddToCart custom functions
require get_template_directory() . '/includes/wc-functions-add-to-cart.php';
// My Account custom functions
require get_template_directory() . '/includes/wc-functions-myaccount.php';
// Cart|Mini-cart custom functions
require get_template_directory() . '/includes/wc-functions-cart.php';
// Checkout custom functions 
require get_template_directory() . '/includes/wc-functions-checkout.php';

// Chagne currency symbol to text
add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);
function change_existing_currency_symbol( $currency_symbol, $currency ) {
    switch( $currency ) {
        case 'UAH': $currency_symbol = 'грн'; break;
    }
    return $currency_symbol;
}

// Change wc_get_products function with specific parameter given
add_filter('woocommerce_product_data_store_cpt_get_products_query', 'get_products_for_front_page', 10, 2);
function get_products_for_front_page($query, $query_vars) {
    if (!empty($query_vars['show_on_front'])) {
        $args = array(
            'hide_empty' => false, // also retrieve terms which are not used yet
            'meta_query' => array(
                array(
                    'key'       => 'dhl_category_on_front',
                    'value'     => 1,
                    'compare'   => '='
                )
            ),
           'taxonomy'  => 'product_cat',
        );
        $terms = get_terms($args);
        if (!empty($terms)) {
            $term = array_shift($terms);
            unset($terms);
            $query['tax_query'][] = array(
                'taxonomy' => 'product_cat',
                'field'    => 'dhl_category_on_front',
                'terms'    =>  array($term->term_id),
                'operator' => 'IN',
            );
        }
    }
    return $query;
}

/**
 * Ajax event products_by_size
 * Retrieve products by size slug
 */
add_action( 'wp_ajax_products_by_size', 'getProductsBySize');
add_action( 'wp_ajax_nopriv_products_by_size', 'getProductsBySize');
function getProductsBySize() {
    
    if (empty($_POST['slug'])) {
        wp_send_json(array('error' => 'Empty slug given'));
    }
    if (empty($_POST['elementsCount'])) {
        $_POST['elementsCount'] = 4;
    }
    $res = array();
    $products = wc_get_products(array(
        'posts_per_page' => $_POST['elementsCount'],
        'show_on_front' => true,
        'paginate' => true,
        'tax_query' => array(
            array(
                'taxonomy' => 'pa_size',
                'field'    => 'slug',
                'terms'    =>  array($_POST['slug']),
                'operator' => 'IN',
            )
        )
    ));
	

    if ($_POST['elementsCount'] >= $products->total) {
        $show_load_more = false;
    } else {
        $show_load_more = true;
    }
    
    if (!empty($products)) {
        foreach ($products->products as $idx => $product) {
            $price = $product->get_price();
            foreach ($product->get_available_variations() as $variation){
                if($variation['attributes']['attribute_pa_size'] == $_POST['slug']){
                    $price = $variation['display_price'];
                    break;
                }
            }
            $res[$idx]['permalink'] = $product->get_permalink() . '#' . $_POST['slug'];
            $res[$idx]['title'] = $product->get_title();
            $res[$idx]['img'] = wp_get_attachment_image_url($product->get_image_id(), 'large');
            $res[$idx]['price'] = $price;
        }
    }

	wp_send_json(array('products' => $res, 'show_load_more' => $show_load_more));
    wp_die();
}

// Add shop page to breadcrumbs
add_filter( 'woocommerce_get_breadcrumb', function($crumbs, $Breadcrumb){
    $shop_page_id = wc_get_page_id('shop'); //Get the shop page ID
    
    if (is_account_page() || is_cart() || is_checkout() || is_page()) {
        return $crumbs;
    }
    
    if($shop_page_id > 0 && !is_shop() || is_product_category() || is_product() ) { //Check we got an ID (shop page is set). Added check for is_shop to prevent Home / Shop / Shop as suggested in comments
        $new_breadcrumb = [
            get_the_title($shop_page_id), //Title
            get_permalink($shop_page_id) // URL
        ];
        array_splice($crumbs, 1, 0, [$new_breadcrumb]); //Insert a new breadcrumb after the 'Home' crumb
    }
    return $crumbs;
}, 10, 2 );



/**
 * Ajax event products_by_size
 * Retrieve products by size slug
 */
add_action( 'wp_ajax_product_quantity_update', 'productQuantityUpdate');
add_action( 'wp_ajax_nopriv_product_quantity_update', 'productQuantityUpdate');
function productQuantityUpdate() {
    
    if (empty($_POST['productKey'])) {
        wp_send_json(array('error' => 'Empty key given'));
    }
    
    WC()->cart->set_quantity($_POST['productKey'], $_POST['quantity'], true );

    wp_send_json(WC_AJAX::get_refreshed_fragments());
    wp_die();
}
