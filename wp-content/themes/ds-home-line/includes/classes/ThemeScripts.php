<?php 
/**
 * @package DsHomeLine
 * @since 1.0
 * @version 1.0
 */
if ( !defined('ABSPATH') ) {
    exit;
}

if ( !class_exists('ThemeScripts') ) {
    
    /**
     * Main theme scripts configurations class
     * @author Perfectorium
     */
    class ThemeScripts {
        
        /**
         * Hold class intstance
         * @var object
         */
        private static $_instance = null;
        
        /**
         * Define theme settings class instance
         * @return object|ThemeScripts
         */
        public static function instance()
        {
            if ( is_null(self::$_instance) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }
        
        public function __construct()
        {
            add_action('wp_head', array($this, 'enqueueMeta'));
            add_action('wp_enqueue_scripts', array($this, 'enqueueScripts'));
            add_action('wp_enqueue_scripts', array($this, 'enqueueStyles'));
            add_action( 'admin_enqueue_scripts', array($this, 'enqueueAdminScripts'));
        }
        
        
        public function enqueueAdminScripts()
        {
            wp_enqueue_script( 'wp-color-picker');
            wp_enqueue_style( 'wp-color-picker');
        }
        
        public function enqueueScripts()
        {
            if ( !is_admin() ) {
                wp_deregister_script('jquery');
                wp_register_script('jquery', THEME_URL. '/assets/front-end/app/js/jquery-3.4.1.min.js', false, null, true);
                wp_enqueue_script('jquery');
            }
            wp_enqueue_script('libs-js', THEME_URL . '/assets/front-end/app/js/libs.min.js', array(), '20192019', true);
            wp_enqueue_script('common-js', THEME_URL . '/assets/front-end/app/js/common.js', array('jquery'), null, true);
            
            wp_enqueue_script('scrips-js', THEME_URL . '/assets/js/script.js', array('jquery'), null, true);
            wp_enqueue_script('validate-js', THEME_URL . '/assets/js/validate-settings.js', array('jquery'), null, true);
            wp_localize_script('scrips-js', 'ajaxData',
                array (
                    'url' => admin_url('admin-ajax.php')
                )
            );

            // Custom add to cart javascript
            if ( is_product() ) {
                wp_enqueue_script('add-to-cart-js', THEME_URL . '/assets/js/add-to-cart.js', array('jquery'), null, true);
                wp_localize_script('add-to-cart-js', 'ajaxData',
                    array (
                        'url' => admin_url('admin-ajax.php')
                    )
                );
                wp_localize_script('add-to-cart-js', 'productAttrHideValues',
                    array (
                        'by' => carbon_get_theme_option('attr_hide_by'),
                        'color' => carbon_get_theme_option('attr_hide_color')
                    )
                );
            }
            // Custom templates update js
            if ( is_front_page() || is_product_category() ) {
                wp_enqueue_script('wp-util');
                wp_enqueue_script('tmlp-js', THEME_URL . '/assets/js/tmpl.js', array('wp-util'), null, true);
            }
            // Custom checkout js
            if ( is_checkout() ) {
                wp_enqueue_script('checkout-js', THEME_URL . '/assets/js/checkout.js', array('jquery'), null, true);
            }
        }
        
        public function enqueueStyles()
        {
            if ( is_admin() ) {
               
            }
            wp_enqueue_style(
                'libs-style',
                THEME_URL . '/assets/front-end/app/css/libs.min.css',
                array(),
                (file_exists(THEME_PATH . '/assets/front-end/app/css/libs.min.css')) ? filemtime(THEME_PATH . '/assets/front-end/app/css/libs.min.css') : '1.0.0'
            );
            wp_enqueue_style(
                'main-style',
                THEME_URL . '/assets/front-end/app/css/style.css',
                array(),
                (file_exists(THEME_PATH . '/assets/front-end/app/css/style.css')) ? filemtime(THEME_PATH . '/assets/front-end/app/css/style.css') : '1.0.0'
            );
            wp_enqueue_style('style-wp', get_stylesheet_uri());
        }
        
        public function enqueueMeta()
        {
            echo '<meta charset="'. get_bloginfo( 'charset' ) . '">';
            echo '<meta name="format-detection" content="telephone=no">';
            echo '<meta http-equiv="X-UA-Compatible" content="IE=edge" />';
            echo '<meta name="viewport"  content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">';
        }
        
    }
    
}
ThemeScripts::instance();