<?php 
/**
 * WooCommerce custom fields
 * 
 * @package DsHomeLine
 * @since 1.0
 * @version 1.0
 */
defined( 'ABSPATH' ) || exit;

if ( !class_exists('WooCustomFields') ) {
    
    /**
     * Main theme configurations class
     * @author Perfectorium
     */
    class WooCustomFields 
    {
        
        /**
         * Hold class intstance
         * @var object
         */
        private static $_instance = null;
        
        /**
         * Custom fields declaration
         * @see https://github.com/woocommerce/woocommerce/blob/master/includes/admin/wc-meta-box-functions.php
         * @var array
         */
        private $fields = array(
            'dhl_variations_slider_status' => array(
                'id' => 'dhl_variations_slider_status',
                'name' => 'dhl_variations_slider_status',
                'class' => self::CUSTOM_FIELD_CLASS,
                'cbvalue' => 1,
                'field_type' => 'checkbox',
                'label' => 'Слайдер из торговых предложений',
                'description' => 'Веключить данную опцию чтобы создавать слайдер из изображений торговых предложений'
            ),
            'dhl_complect_color' => array(
                'id' => 'dhl_complect_color',
                'name' => 'dhl_complect_color',
                'class' => self::CUSTOM_FIELD_CLASS,
                'field_type' => 'text',
                'label' => 'Цвет комплекта 1',
                //'desc_tip' => true,
                'description' => 'Заполняется для комплектов, в противном случае оставить пустым.'
            ),
            'dhl_complect_color_2' => array(
                'id' => 'dhl_complect_color_2',
                'name' => 'dhl_complect_color_2',
                'class' => self::CUSTOM_FIELD_CLASS,
                'field_type' => 'text',
                'label' => 'Цвет комплекта 2',
                //'desc_tip' => true,
                'description' => 'Если цвет товара составной(из двух цветов) заполните это поле.'
            ),
        );
        
        /**
         * Used in WooCustomFields::addProductTaxonomyCustomMeta() method
         * @var array
         */
        private $taxonomyFields = array(
            'dhl_category_on_front' => array(
                'id' => 'dhl_category_on_front',
                'name' => 'dhl_category_on_front',
                'type' => 'checkbox',
                'title' => 'Использовать на главной',
                'description' => 'Отображать категорию товаров на главной странице',
                'value' => 1
            ),
            'dhl_category_custom_link' => array(
                'id' => 'dhl_category_custom_link',
                'name' => 'dhl_category_custom_link',
                'type' => 'text',
                'title' => 'Произвольная ссылка на страницу',
                'description' => 'Заполняется если переход должен осуществлятся не на катетегорию товаров, а на какой-то произвольный URL.',
                'value' => ''
            ),
            'dhl_category_custom_link_status' => array(
                'id' => 'dhl_category_custom_link_status',
                'name' => 'dhl_category_custom_link_status',
                'type' => 'checkbox',
                'title' => 'Использовать произвольную ссылку',
                'description' => '',
                'value' => 1
            )
        );
        
        /**
         * Woocommerce custom field class
         * @var string
         */
        const CUSTOM_FIELD_CLASS = 'dhl-custom-field';
        
        /**
         * Define theme settings class instance
         * @return object|WooCustomFields
         */
        public static function instance()
        {
            if ( is_null(self::$_instance) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }
        
        public function __construct()
        {
            // Woocommerce custom fields saving
            add_action('woocommerce_process_product_meta', array(&$this, 'saveWooCustomFields'));
            
            // Woocommerce advanced tab custom fields hook
            add_action('woocommerce_product_options_advanced', array(&$this, 'advancedTabFields'));
            
            // Woocommerce custom fields for product category taxonomy
            add_action( 'product_cat_edit_form_fields', array(&$this, 'addProductTaxonomyCustomMeta'), 10, 2);
            
            // Woocommerce SAVE Custom fields for PRODUCT CATEGORY
            add_action( 'edited_product_cat', array(&$this, 'saveProductTaxonomyCustomMeta'), 10, 2 );
            add_action( 'create_product_cat', array(&$this, 'saveProductTaxonomyCustomMeta'), 10, 2 );
            
        }
        
        // Create advanced tab custom fields for single product 
        public function advancedTabFields() 
        {
            // dhl_variations_slider_status field
            ?><div class="options_group"><?php woocommerce_wp_checkbox($this->fields['dhl_variations_slider_status']);?></div><?php
            
            // dhl_complect_color field
            ?><div class="options_group"><?php woocommerce_wp_text_input($this->fields['dhl_complect_color']);?></div><?php
            ?><div class="options_group"><?php woocommerce_wp_text_input($this->fields['dhl_complect_color_2']);?></div><?php
            ?>
            <script>
    		jQuery(document).ready(function($){
    		    $('#dhl_complect_color, #dhl_complect_color_2').each(function(){
            		$(this).wpColorPicker();
    		    });
    		});
    		</script>
            <?php
        }
        
        // Woocommerce cutoms fields save function
        public function saveWooCustomFields($postID)
        {
            $product = wc_get_product($postID);
            
            foreach ($this->fields as $field) {
                
                switch ($field['field_type']) {
                    case 'checkbox': 
                        $val = isset($_POST[$field['name']]) ? $_POST[$field['name']] : 0;
                        $product->update_meta_data($field['name'], $val);
                        break;
                    case 'text':
                        $val = isset($_POST[$field['name']]) ? $_POST[$field['name']] : '';
                        $product->update_meta_data($field['name'], $val);
                        break;
                }
                
            }
            
            $product->save();
        }
        
        // Creating custom fields html for product taxonomy
        public function addProductTaxonomyCustomMeta($term)
        {
            foreach ($this->taxonomyFields as $field) {
                
                switch ($field['type']) {
                    case 'checkbox':
                        $checked = get_term_meta($term->term_id, $field['name'], true);
                        ?>
                    	<tr class="form-field">
                    		<th scope="row" valign="top">
                    			<label for="<?php echo $field['id']; ?>"><?php echo $field['title']; ?></label>
                			</th>
                    		<td>
                    			<input type="<?php echo $field['type']; ?>" name="<?php echo $field['name']; ?>" id="<?php echo $field['id']; ?>" value="<?php echo $field['value']; ?>" <?php checked($checked, $field['value'], true); ?>>
                    			<p class="description"><?php echo $field['description']; ?></p>
                    		</td>
                    	</tr>
                		<?php
                		break;
            		default:
            		    ?>
                    	<tr class="form-field">
                    		<th scope="row" valign="top">
                    			<label for="<?php echo $field['id']; ?>"><?php echo $field['title']; ?></label>
                			</th>
                    		<td>
                    			<input type="<?php echo $field['type']; ?>" name="<?php echo $field['name']; ?>" id="<?php echo $field['id']; ?>" value="<?php echo get_term_meta($term->term_id, $field['name'], true); ?>">
                    			<p class="description"><?php echo $field['description']; ?></p>
                    		</td>
                    	</tr>
                		<?php
                		break;
                }
            }
        }
        
        // Saving custom fields html for product taxonomy
        public function saveProductTaxonomyCustomMeta($termID)
        {
            foreach ($this->taxonomyFields as $field) {
                switch ($field['type']) {
                    case 'checkbox':
                        $val = isset($_POST[$field['name']]) ? $_POST[$field['name']] : 0;
                        update_term_meta($termID, $field['name'], $val);
                        break;
                    case 'text':
                        $val = isset($_POST[$field['name']]) ? $_POST[$field['name']] : '';
                        update_term_meta($termID, $field['name'], $val);
                        break;
                }
            }
        }
        
    } // END CLASS
    
}
WooCustomFields::instance();