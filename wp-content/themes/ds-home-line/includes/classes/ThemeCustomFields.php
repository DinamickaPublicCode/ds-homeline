<?php 
/**
 * @package DsHomeLine
 * @since 1.0
 * @version 1.0
 */
if ( !defined('ABSPATH') ) {
    exit;
}

use Carbon_Fields\Container;
use Carbon_Fields\Field;

if ( !class_exists('ThemeCustomFields') ) {
    
    /**
     * Main theme configurations class
     * @author Perfectorium
     */
    class ThemeCustomFields {
        
        /**
         * Hold class intstance
         * @var object
         */
        private static $_instance = null;
        
        /**
         * @link https://carbonfields.net/docs/containers-theme-options/
         * @var null|object
         */
        private $globalContainer = null;
        
        /**
         * Define theme settings class instance
         * @return object|ThemeCustomFields
         */
        public static function instance()
        {
            if ( is_null(self::$_instance) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }
        
        public function __construct()
        {
            $this->bootCarbonFields();
        }
        
        /**
         * Boot CarbonFields library
         * Fields/Containers registration
         * @link https://docs.carbonfields.net/
         * @link https://docs.carbonfields.net/#/quickstart
         */
        private function bootCarbonFields()
        {
            /**
             * Boot CarbonFields library
             * @link https://docs.carbonfields.net/#/
             */
            add_action( 'after_setup_theme', function() {
                require_once( THEME_PATH . '/includes/libs/carbon-fields/vendor/autoload.php' );
                \Carbon_Fields\Carbon_Fields::boot();
            } );
            
            //Global theme custom fields - theme options
            add_action('carbon_fields_register_fields', array(&$this, 'globalSettings'), 10);
            //Front page custom fields
            add_action('carbon_fields_register_fields', array(&$this, 'frontPageSettings'), 20);
            //Review post_type custom fields
            add_action('carbon_fields_register_fields', array(&$this, 'reviewSettings'), 30);
            // Poducts size table
            add_action('carbon_fields_register_fields', array(&$this, 'sizeTableSettings'), 40);
            
            // Common sections settings
            add_action('carbon_fields_register_fields', array(&$this, 'sectionsSettings'), 40);
        }
        
        
        /*
         * Register theme global options via CarbonFields
         * Header|Footer|Menu options
         */
        public function globalSettings()
        {
            $this->globalContainer = Container::make( 'theme_options', "Global Settings" )
            ->set_page_menu_position(4)
            ->add_fields(array(
                Field::make('html', 'html1')->set_html('<h2 style="background: lightsteelblue; text-align: center;"><strong>Header</strong></h2>'),
                Field::make('image', 'header_logo', 'Logo')
            ))
            ->add_fields(array(
                Field::make('html', 'html2')->set_html('<h2 style="background: lightsteelblue; text-align: center;"><strong>Menu</strong></h2>'),
                Field::make('image', 'menu_logo', 'Logo')->set_width(20),
                Field::make('complex', 'menu_social', 'Socials')
                ->set_collapsed(true)
                ->set_width(80)
                ->add_fields(array(
                    Field::make('select', 'type', 'Icon Type')->set_width(50)->set_options( array(
                        'fb' => 'Facebook',
                        'inst' => 'Instagram',
                        'phone' => 'Phone',
                        'env' => 'Envelope',
                    ) ),
                    Field::make('text', 'href', 'Link')->set_width(50)
                )),
            ))
            ->add_fields(array(
                Field::make('html', 'html3')->set_html('<h2 style="background: lightsteelblue; text-align: center;"><strong>Footer</strong></h2>'),
                Field::make('text', 'footer_email', 'Email')->set_width(20),
                Field::make('text', 'footer_phone', 'Phone')->set_width(20),
                Field::make('complex', 'footer_social', 'Socials')
                ->set_collapsed(true)
                ->set_width(60)
                ->add_fields(array(
                    Field::make('select', 'type', 'Icon Type')->set_width(50)->set_options( array(
                        'fb' => 'Facebook',
                        'inst' => 'Instagram',
                    ) ),
                    Field::make('text', 'href', 'Link')->set_width(50)
                )),
            ))
            ->add_fields(array(
                Field::make('html', 'html4')->set_html('<h2 style="background: lightsteelblue; text-align: center;"><strong>Advanced</strong></h2>'),
                Field::make('text', 'dostavka_oplata_page_link', 'Ссылка на страницу оплаты и доставки')->set_width(20),
            ))
            ->add_fields(array(
                Field::make('html', 'html5')->set_html('<h2 style="background: lightsteelblue; text-align: center;"><strong>Woocommerce Product Page</strong></h2>'),
                Field::make('text', 'attr_hide_by', 'Ярылк атрибута по которому прячем')
                ->set_default_value('type-of-fabric')
                ->set_width(20),
                Field::make('text', 'attr_hide_color', 'Ярылк атрибута "Цвет" который будем прятать')
                ->set_default_value('color')
                ->set_width(20),
               
            ));
           
            Container::make( 'nav_menu_item', __( 'Menu Settings' ) )
            ->add_fields( array(
                Field::make( 'complex', 'menu_products', __( 'Add Product' ) )
                ->add_fields(array(
                    Field::make('text', 'id', 'ID')                    
                ))->set_collapsed(true),
            ));
          
        }
        
        public function sectionsSettings()
        {
            Container::make('theme_options', 'Sections')
            ->set_page_parent($this->globalContainer)
            ->add_fields(array(
                Field::make('html', 'html1')->set_html('<h2 style="background: lightsteelblue; text-align: center;"><strong>Блок - "Найди комлект"</strong></h2>'),
                Field::make('text', 'fyc_title', 'Заголовок')->set_width(50),
                Field::make('text', 'fyc_link', 'Ссылка')->set_width(50),
                Field::make('rich_text', 'fyc_description', 'Описание'),
            ))
            ->add_fields(array(
                Field::make('html', 'html2')->set_html('<h2 style="background: lightsteelblue; text-align: center;"><strong>Блок - "Обратная связь"</strong></h2>'),
                Field::make('text', 'cu_title', 'Заголовок')->set_width(50),
                Field::make('text', 'cu_subtitle', 'Подзаголовок')->set_width(50),
                Field::make('text', 'cu_form_code', 'Шорткод формы'),
            ))
            ->add_fields(array(
                Field::make('html', 'html3')->set_html('<h2 style="background: lightsteelblue; text-align: center;"><strong>Блок - "Слайдер"</strong></h2>'),
                Field::make('complex', 'overview_slider', '')
                ->set_collapsed(true)
                ->add_fields(array(
                    Field::make('text', 'title', 'Заголовок'),
                    Field::make('rich_text', 'desc', 'Описание')->set_width(70),
                    Field::make('image', 'img', 'Изображение')->set_width(30),
                ))
                ->set_header_template('
                    <% if (title) { %>
                        <%- title %>
                    <% } else { %>
                        Новый слайд
                    <% } %>
                '),
            ))
            ->add_fields(array(
                Field::make('html', 'html4')->set_html('<h2 style="background: lightsteelblue; text-align: center;"><strong>Блок - "Купить в один клик"</strong></h2>'),
                Field::make('text', 'bof_form_code', 'Шорткод формы'),
            ));
        }
        
        /**
         * Front-page options
         */
        public function frontPageSettings()
        {
            Container::make('post_meta', 'Front Page')
            ->where('post_template', '=', 'templates/homepage.php')
            ->where('post_type', '=', 'page')
            ->add_fields(array(
                Field::make('html', 'html1')->set_html('<h2 style="background: lightsteelblue; text-align: center;"><strong>Section - "Top"</strong></h2>'),
                Field::make('text', 'top_title', 'Title')->set_width(30),
                Field::make('rich_text', 'top_description', 'Description')->set_width(70),
            ))
            ->add_fields(array(
                Field::make('html', 'html2')->set_html('<h2 style="background: lightsteelblue; text-align: center;"><strong>Section - "About Us"</strong></h2>'),
                Field::make('text', 'au_title', 'Title')->set_width(30),
                Field::make('rich_text', 'au_description', 'Description')->set_width(70),
                Field::make('complex', 'au_advantages', 'Advantages')
                ->set_collapsed(true)
                ->add_fields(array(
                    Field::make('text', 'title', 'Title')->set_width(60),
                    Field::make('select', 'icon', 'Icon')->set_width(40)->set_options(array(
                        'gift' => 'Gift',
                        'diamond' => 'Diamond',
                        'gift-box' => 'GiftBox'
                    ) ),
                ))->set_header_template('
                    <% if (title) { %>
                        <%- title %>
                    <% } else { %>
                        New Advantage
                    <% } %>
                '),
            ))
            ->add_fields(array(
                Field::make('html', 'html3')->set_html('<h2 style="background: lightsteelblue; text-align: center;"><strong>Section - "Buy by Category"</strong></h2>'),
                Field::make('text', 'bbk_title', 'Title'),
            ))
            ->add_fields(array(
                Field::make('html', 'html5')->set_html('<h2 style="background: lightsteelblue; text-align: center;"><strong>Section - "Reviews"</strong></h2>'),
                Field::make('text', 'rw_title', 'Title'),
                Field::make('association', 'rw_association', 'Reviews')
                ->set_types(array(
                    array(
                        'type'      => 'post',
                        'post_type' => 'review',
                    )
                ))
            ))
            ->add_fields(array(
                Field::make('html', 'htmllast')->set_html('<h2 style="background: lightsteelblue;"></h2>'),
            ));
        }
        
        /**
         * Review post_type custom fields
         */
        public function reviewSettings()
        {
            Container::make('post_meta', 'Properties')
            ->where('post_type', '=', 'review')
            ->set_context('side')
            ->set_priority('low')
            ->add_fields(array(
                Field::make('html', 'html1')->set_html('<h2 style="background: lightsteelblue; text-align: center;"><strong>Social</strong></h2>'),
                Field::make( 'text', 'instagram', 'Instagram' ),
            ))
            ->add_fields(array(
                Field::make('html', 'html2')->set_html('<h2 style="background: lightsteelblue; text-align: center;"><strong>Display Type</strong></h2>'),
                Field::make( 'select', 'display_type', 'Type' )->set_options(array(
                    'text' => 'Text',
                    'photo' => 'Photo',
                )),
                Field::make('image', 'photo', 'Photo')
                ->set_help_text('Select photo to be displayed on review instead of text.')
                ->set_conditional_logic(array(
                    array(
                        'field' => 'display_type',
                        'value' => 'photo',
                    )
                ))
            ))
            ->add_fields(array(
                Field::make('html', 'htmllast')->set_html('<h2 style="background: lightsteelblue; text-align: center;"></h2>'),
            ));
        }
        
        
        public function sizeTableSettings()
        {
            Container::make('theme_options', 'Size table')
            ->set_page_parent($this->globalContainer)
            ->add_fields(array(
                Field::make('html', 'html2')->set_html('<h2 style="background: lightsteelblue; text-align: center;"><strong>Таблица размеров</strong></h2>'),
                Field::make('complex', 'size_table', '')
                ->set_collapsed(true)
                ->add_fields(array(
                    Field::make('text', 'size', 'Размер')->set_width(50),
                    Field::make('text', 'slug', 'Ярлык атрибута')->set_width(50),
                    Field::make('text', 'duvet_cover', 'Пододеяльник')->set_width(25),
                    Field::make('text', 'tension_sheet', 'Натяжная простыня')->set_width(25),
                    Field::make('text', 'sheet', 'Простыня')->set_width(25),
                    Field::make('text', 'pillowcase', 'Наволочки')->set_width(25),
                ))
                ->set_header_template('
                    <% if (size) { %>
                        <%- size %>
                    <% } else { %>
                        Новый размер
                    <% } %>
                '),
            ));
        }
        //END CLASS
    }
    
}
ThemeCustomFields::instance();