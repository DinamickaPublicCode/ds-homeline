<?php 
/**
 * @package DsHomeLine
 * @since 1.0
 * @version 1.0
 */
if ( !defined('ABSPATH') ) {
    exit;
}

if ( !class_exists('ThemeSettings') ) {
    
    /**
     * Main theme configurations class
     * @author Perfectorium
     */
    class ThemeSettings {
        
        /**
         * Hold class intstance
         * @var object
         */
        private static $_instance = null;
        
        /**
         * Define theme settings class instance
         * @return object|ThemeSettings
         */
        public static function instance()
        {
            if ( is_null(self::$_instance) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }
        
        /**
         * Default class constructor
         * @return void
         */
        public function __construct() 
        {
            $this->defineVariables();
            $this->supports();
            $this->filters();
            $this->actions();
            $this->menusRegistration();
        }
        
        /**
         * Define theme global variables
         */
        private function defineVariables()
        {
            if ( !defined('THEME_PATH') ) {
                define('THEME_PATH', get_template_directory());
            }
            if ( !defined('THEME_URL') ) {
                define('THEME_URL', get_template_directory_uri());
            }
        }
        
        /**
         * Setup theme dependencies extensions
         * @link https://developer.wordpress.org/reference/functions/add_theme_support/
         */
        private function supports()
        {
            /**
             * Enable woocommerce plugin
             * @link https://blog.templatetoaster.com/create-woocommerce-theme/
             */
            add_theme_support('woocommerce');
            /**
             * Enable support for Post Thumbnails on posts and pages.
             * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
             */
            add_theme_support('post-thumbnails');
            /**
             * Enable support for Menus
             */
            add_theme_support('menus');
            /**
             * Enable support for Widgets
             */
            add_theme_support('widgets');
            /*
             * Let WordPress manage the document title.
             * By adding theme support, we declare that this theme does not use a
             * hard-coded <title> tag in the document head, and expect WordPress to
             * provide it for us.
             */
            add_theme_support('title-tag');
            
        }
        
        /**
         * Add theme filters
         * @link https://developer.wordpress.org/reference/functions/add_filter/
         */
        private function filters()
        {
            //Enable mime types upload
            add_filter('upload_mimes', function($mimes) {
                $mimes['svg'] = 'image/svg+xml';
                return $mimes;
            });
            
            //Add Thumbnail image to review post_type list (admin dashboard)
            add_filter('manage_review_posts_columns', function ($columns) {
                $columns = array(
                    'cb' => '<input type="checkbox" />',
                    'featured_image' => 'Photo',
                    'title' => 'Title',
                    'comments' => '<span class="vers"><div title="Comments" class="comment-grey-bubble"></div></span>',
                    'date' => 'Date'
                );
                return $columns;
            });
                
        }
        
        /**
         * Add theme actions
         * @link https://developer.wordpress.org/reference/functions/add_action/
         */
        private function actions()
        {
            //Hide admin panel on front side
            add_action('show_admin_bar', '__return_false');
            
            //Thumbnail on posts list page (admin dashboard)
            add_action('manage_posts_custom_column' , function ($column, $post_id) {
                switch ($column) {
                    case 'featured_image':
                        the_post_thumbnail(array(50, 50), array('style' => 'border-radius: 100px;'));
                        break;
                }
            }, 10, 2);
        }
        
        /**
         * Register navigation menus
         * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
         */
        private function menusRegistration()
        {
            register_nav_menus( array(
                'main_menu' => 'Desktop Menu',
                'footer_1' => 'Footer 1',
                'footer_2' => 'Footer 2',
                'footer_3' => 'Footer 3',
                'footer_4' => 'Footer 4',
            ) );
        }
        
    }
    
}
ThemeSettings::instance();