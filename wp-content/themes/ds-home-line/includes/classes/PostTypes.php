<?php 
/**
 * @package DsHomeLine
 * @since 1.0
 * @version 1.0
 */
if ( !defined('ABSPATH') ) {
    exit;
}

if ( !class_exists('PostTypes') ) {
    
    /**
     * Main theme post_types configurations class
     * @author Perfectorium
     */
    final class PostTypes {
        
        /**
         * Hold class intstance
         * @var object
         */
        private static $_instance = null;
        
        /**
         * Define theme settings class instance
         * @return object|PostTypes
         */
        public static function instance()
        {
            if ( is_null(self::$_instance) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }
        
        public function __construct()
        {
            $this->registerReviewType();
        }
        
        /**
         * Custom post_type = 'reviews' registration
         * @link https://developer.wordpress.org/reference/functions/register_post_type/
         */
        private function registerReviewType()
        {
            add_action('init', function() {
                register_post_type('review', array(
                    'label' => null,
                    'labels' => array(
                        'name' => 'Отзывы',
                        'singular_name' => 'Отзыв',
                        'add_new' => 'Добавить отзыв',
                        'add_new_item' => 'Создание отзыва',
                        'edit_item' => 'Редактировать',
                        'new_item' => 'Новый отзыв ',
                        'view_item' => 'Просмотр',
                        'search_items' => 'Поиск отзыва',
                        'not_found' => 'Не найдено',
                        'not_found_in_trash' => 'Не найдено в корзине',
                        'all_items' => 'Все отзывы',
                        'menu_name' => 'Отзывы'
                    ),
                    'description' => 'Review post type.',
                    'public' => false,
                    'publicly_queryable' => false,
                    'exclude_from_search' => true,
                    'show_ui' => true,
                    'show_in_menu' => true,
                    'show_in_nav_menus' => false,
                    'show_in_rest' => false,
                    'menu_position' => 25,
                    'menu_icon' => 'dashicons-testimonial',
                    'hierarchical' => false,
                    'supports' => array('title','editor','thumbnail'),
                    'taxonomies' => array(),
                    'has_archive' => false,
                    'rewrite' => false,
                    'query_var' => false
                ));
            });
        }
        
    }
    
}
PostTypes::instance();