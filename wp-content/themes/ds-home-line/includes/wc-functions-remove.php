<?php
/**
 * Actions|Filters removing standart functional of woocommerce
 * @package DsHomeLine
 * @since 1.0
 * @version 1.0
 */
defined( 'ABSPATH' ) || exit;

// Remove default woocommerce styles in one hook
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );