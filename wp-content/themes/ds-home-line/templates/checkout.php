<?php 
/**
 * Template Name: Checkout
 * @package DsHomeLine
 * @since 1.0
 */
defined( 'ABSPATH' ) || exit;
?>
<?php get_header(); ?>
<?php 
if ( have_posts() ) {
    
    while( have_posts() ) {
        
	    the_post();
	    the_content();
	    
    };
    
}
?>
<?php get_footer(); ?>