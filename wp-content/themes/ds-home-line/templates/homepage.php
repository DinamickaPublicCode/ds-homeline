<?php 
/**
 * Template Name: Homepage
 * @package DsHomeLine
 * @since 1.0
 */
defined( 'ABSPATH' ) || exit;
?>
<?php get_header(); ?>

<?php get_template_part('template-parts/homepage/section', 'top'); ?>

<?php get_template_part('template-parts/homepage/section', 'about'); ?>

<?php get_template_part('template-parts/homepage/section', 'products'); ?>

<?php get_template_part('template-parts/common/section', 'find-complect'); ?>

<?php get_template_part('template-parts/homepage/section', 'reviews'); ?>

<?php get_template_part('template-parts/common/section', 'contact'); ?>

<?php get_footer(); ?>