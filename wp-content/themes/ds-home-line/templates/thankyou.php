<?php
/**
 * Template Name: Thankyou Page
 */
get_header();
?>
<section class="layout__sect">
    <div class="layout-alert__img">
        <img src="<?php echo get_theme_file_uri()?>/assets/front-end/app/img/logo/alertEl.svg" alt="DS HOME LINE">
    </div>
    <div class="container">
        <div class="layout__title">
            <h3>Благодарим за заказ:)</h3>
        </div>
        <div class="layout__info">
            <div class="full-content">
                <p>В скором времени мы свяжемся с Вами</p>
            </div>
            <a  href="/" class="btn">На главную</a>
        </div>
    </div>
</section>
<?php
get_footer();