<?php 
/**
 * The Template for displaying mini-cart block
 *
 * @package DsHomeLine
 * @version 1.0
 */
defined( 'ABSPATH' ) || exit;
?>
<div class="header__item">
    <div class="cart" id="cart" data-get-popup="cart-tbl">
        <?php 
        if ( function_exists('dhl_woocommerce_cart_icon') ) {
            dhl_woocommerce_cart_icon(); 
        }
        ?>
    </div>
</div>