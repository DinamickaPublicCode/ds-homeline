<?php 
/**
 * The Template for displaying overview section
 *
 * @package DsHomeLine
 * @version 1.0
 */
defined( 'ABSPATH' ) || exit;
if ( function_exists('carbon_get_theme_option') ) {
    $slider = carbon_get_theme_option('overview_slider');
?>

	<?php if (!empty($slider)) { ?>
	
    <section class="layout__sect">
    	<div class="container">
    		<div class="overview-wrap slick-carousel">
    			<?php foreach ($slider as $slide) { ?>
    			<div class="overview">
    				<div class="overview__blank">
    					<div class="overview__blank-wrap">
    						<div class="overview__blank-img">
    							<?php echo wp_get_attachment_image($slide['img'], 'large', false, array('class' => 'object-fit is--cover', 'data-object-fit' => 'cover')); ?>
    						</div>
    					</div>
    					<div class="overview__blank-text full--content">
    						<h3><span><?php echo $slide['title']; ?></span></h3>
    						<?php echo wpautop($slide['desc']); ?>
    					</div>
    				</div>
    			</div>
    			<?php } ?>
    
    		</div>
    	</div>
    </section>
    
    <?php } ?>
    
<?php } ?>