<?php 
defined( 'ABSPATH' ) || exit;

if ( function_exists('carbon_get_theme_option') ) {
    
    $title = carbon_get_theme_option('fyc_title');
    $desc = carbon_get_theme_option('fyc_description');
    $link = carbon_get_theme_option('fyc_link');
?>
<section class="layout__sect layout__sect--secondary">
	<div class="container">
		<div class="layout__title">
			<h2><?php echo $title; ?></h2>
		</div>
		<div class="layout__info">
			<div class="full-content">
				<?php echo $desc; ?>
			</div>
			<a href="<?php echo $link; ?>" class="btn">Найти</a>
		</div>
	</div>
</section>
<?php } ?>