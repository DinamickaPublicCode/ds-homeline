<?php
defined( 'ABSPATH' ) || exit;
if ( function_exists('carbon_get_theme_option') ) {
    $title = carbon_get_theme_option('cu_title');
    $subTitle = carbon_get_theme_option('cu_subtitle');
    $form = carbon_get_theme_option('cu_form_code');
?>
<section class="layout__sect layout__sect--third">
	<div class="container">
		<div class="layout__title">
			<h2><?php echo $title; ?></h2>
		</div>
		<div class="layout__info">
			<div class="full-content">
				<p><?php echo $subTitle; ?></p>
			</div>
		</div>
		<?php echo do_shortcode($form); ?>
	</div>
</section>
<?php } ?>