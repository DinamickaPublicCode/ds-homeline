<?php 
/**
 * The Template for displaying front-page top section
 *
 * @package DsHomeLine
 * @version 1.0
 */
defined( 'ABSPATH' ) || exit;
?>
<section class="layout__main-sect">

	<div class="layout-row">
	
		<?php get_template_part('template-parts/homepage/top/title'); ?>

		<div class="container">
		
			<div class="container__contains">
			
				<?php get_template_part('template-parts/homepage/top/description') ?>

				<?php get_template_part('template-parts/homepage/top/products') ?>
				
			</div>
			
		</div>
		
	</div>

	<div class="scroll-arrow-wrap">
		<svg class="arrow__icon">
			<use class="arrow__part" xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#scroll-arrow"></use>
		</svg>
	</div>
	
</section>