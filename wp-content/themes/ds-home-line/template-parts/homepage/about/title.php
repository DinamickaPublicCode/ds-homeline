<?php 
defined( 'ABSPATH' ) || exit;
if ( function_exists('carbon_get_post_meta') ) {
    echo sprintf('<div class="layout__title"><h2>%s</h2></div>' , carbon_get_post_meta(get_the_ID(), 'au_title'));
}

