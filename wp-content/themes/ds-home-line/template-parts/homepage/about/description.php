<?php 
defined( 'ABSPATH' ) || exit;
if ( function_exists('carbon_get_post_meta') ) {
    echo sprintf('<div class="full-content">%s</div>' , carbon_get_post_meta(get_the_ID(), 'au_description'));
}