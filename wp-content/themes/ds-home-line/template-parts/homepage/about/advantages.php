<?php 
defined( 'ABSPATH' ) || exit;
if ( function_exists('carbon_get_post_meta') ) {
    $advantages = carbon_get_post_meta(get_the_ID(), 'au_advantages');
?>
<div class="biography">
	<?php if ( !empty($advantages) ) { ?>
    	<ul class="biography-items">
    		<?php foreach ($advantages as $adv) { ?>
    		<li class="biography-item">
    			<div class="biography-icon-wrap">
    				<?php echo getAdvantageIcon($adv['icon']); ?>
    			</div>
    			<div class="biography-text">
    				<p><?php echo $adv['title']; ?></p>
    			</div>
    		</li>
    		<?php } ?>
    	</ul>
	<?php } ?>
</div>
<?php } ?>