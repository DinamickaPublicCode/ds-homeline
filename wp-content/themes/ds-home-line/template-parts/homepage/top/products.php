<?php 
/**
 * The Template for displaying front-page top section - Product Slider
 *
 * @package DsHomeLine
 * @version 1.0
 */
defined( 'ABSPATH' ) || exit;

$products = wc_get_products(array('show_on_front' => true));

if (!empty($products)) {
    $first = $products[0];
?>
<div class="product">
    <div class="product__img">
        <img id="product-img" class="object-fit is--contain" data-object-fit="contain" data-object-position="top" src="<?php echo wp_get_attachment_image_url($first->get_image_id(), 'large'); ?>">
    </div>
</div>
<div class="layout__static-row">
    <div class="layout__wrap">
        <div class="product__items">
            <div class="product__item">
                <a href="<?php echo $first->get_permalink(); ?>" class="product__btn btn" id="product-link"> Купить</a>
            </div>
            <div class="product__item">
                <p><strong><span id="product-price"><?php echo $first->get_price(); ?></span> грн</strong></p>
            </div>
        </div>
        <div class="layout__items">
            <div class="layout__item layout__item-name">
                <p id="product-set"><?php echo $first->get_title(); ?></p>
            </div>

            <div class="layout__item layout__item-select">
                <p>Выберите цвет</p>

                <div class="color-palette-wrap">
                    <div class="color-palette-list slick-carousel">

                        <?php foreach ($products as $product) { ?>
                        <?php 
        				$color_1 = $product->get_meta('dhl_complect_color');
        				$color_2 = $product->get_meta('dhl_complect_color_2');
        				?>
                        <div class="color-palette__item">
                            <div style="background-color: <?php echo $color_1; ?>;" class="color-palette__color"
                                data-image-color="<?php echo wp_get_attachment_image_url($product->get_image_id(), 'large'); ?>"
                                data-price="<?php echo $product->get_price(); ?>"
                                data-set="<?php echo $product->get_title(); ?>"
                                data-href="<?php echo $product->get_permalink(); ?>">
                                <?php if (!empty($color_2)) { ?>
                                <div style="background-color: <?php echo $color_2; ?>;"
                                    class="color-palette__color is--second">
                                </div>
                                <?php }; ?>
                            </div>
                        </div>
                        <?php }; ?>
                    </div>
                    <div class="color-palette-nav">
                        <a href="javascript:void(0);" class="arrow-icon-wrap is--prev" id="arrowPrev">
                            <svg class="arrow__icon">
                                <use class="arrow__part"
                                    xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#left-arrow">
                                </use>
                            </svg>
                        </a>
                        <a href="javascript:void(0);" class="arrow-icon-wrap is--next" id="arrowNext">
                            <svg class="arrow__icon">
                                <use class="arrow__part"
                                    xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#left-arrow">
                                </use>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php }; ?>