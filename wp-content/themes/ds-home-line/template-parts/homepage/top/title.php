<?php 
defined( 'ABSPATH' ) || exit;
if ( function_exists('carbon_get_post_meta') ) {
    $title = carbon_get_post_meta(get_the_ID(), 'top_title');
}
if ( empty($title) ) {
    $title = get_bloginfo('name');
}
?>
<div class="layout__title">
	<div class="container">
		<div class="container__contains">
			<h1><?php echo $title; ?></h1>
		</div>
	</div>
</div>