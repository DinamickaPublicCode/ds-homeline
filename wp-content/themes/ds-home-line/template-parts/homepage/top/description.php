<?php
defined( 'ABSPATH' ) || exit;
if ( function_exists('carbon_get_post_meta') ) {
    $desc = carbon_get_post_meta(get_the_ID(), 'top_description'); ?>
    <div class="layout__info">
    	<div class="full-content">
    		<?php echo $desc; ?>
    	</div>
    </div>
<?php } ?>
