<?php 
defined( 'ABSPATH' ) || exit;
?>
<section class="layout__sect">
	
	<?php get_template_part('template-parts/homepage/reviews/title'); ?>
	
	<?php get_template_part('template-parts/homepage/reviews/review', 'loop'); ?>
	
</section>
