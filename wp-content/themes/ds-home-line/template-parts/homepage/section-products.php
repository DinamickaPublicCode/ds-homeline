<?php 
/**
 * The Template-part for displaying section-product.php - products
 *
 * @package DsHomeLine
 * @version 1.0
 */
defined( 'ABSPATH' ) || exit;

$sizes = carbon_get_theme_option('size_table');
$sizesJson = wp_json_encode( $sizes );
$sizesJsonEsc = _wp_specialchars( $sizesJson, ENT_QUOTES, 'UTF-8', true );
?>
<?php if (!empty($sizes)) { ?>
<section class="layout__sect">

	<?php if (is_product_category()) { ?>
		<?php woocommerce_breadcrumb(); ?>
	<?php }; ?>
	
	<div class="container">
		<div class="layout__title">
			<?php 
			if (is_front_page()) { 
			    echo sprintf('<h2>%s</h2>', 'Покупай по категориям');
			} else {
			    echo sprintf('<h2>%s</h2>', woocommerce_page_title(false));
			};
			?>
		</div>
		<div class="category">
			<div class="category__dropdown">
				<span class="category__dropdown-text"><?php echo $sizes[0]['size']?></span>
				<span class="category__dropdown-icon">
					<svg class="arrow-icon">
						<use class="arrow-icon__part is--dark" xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#right-arrow"></use>
					</svg>
				</span>
			</div>
			<ul class="category__list">
				<?php foreach ($sizes as $idx => $size) { ?>
				<?php 
				if (empty($size['slug'])) { 
				    continue; 
				};
				?>
				<li class="category__item">
					<a class="category__link <?php echo $idx == 0 ? 'is--active' : '';?>" data-slug="<?php echo $size['slug']; ?>" href="javascript:void(0);" >
						<?php echo $size['size']; ?>
					</a>
				</li>
				<?php }; ?>
			</ul>
			
			<div id="sizes-template" class="category__info" data-attributes="<?php echo $sizesJsonEsc;?>"></div>
			<script type="text/html" id="tmpl-sizes-template">
			<p class="category__info-name">Комплект</p>
				<ul class="category__info-descr">
					<li class="category__info-text">
						<p class="category__info-title"><strong>Простынь:</strong></p>
						<p>{{ data.sheet }}</p>
					</li>
					<li class="category__info-text">
						<p class="category__info-title"><strong>Пододеяльник:</strong></p>
						<p>{{ data.duvet_cover }}</p>
					</li>
					<li class="category__info-text">
						<p class="category__info-title"><strong>Наволочка:</strong></p>
						<p>{{ data.pillowcase }}</p>
					</li>
				</ul>
             </p>
            </script>
			
		</div>
	</div>
</section>
<section class="layout__sect products">
	<div class="container">
		<ul id="sizes-products" class="products__row"></ul>
		<script type="text/html" id="tmpl-sizes-products">
            <# for (let i = 0; i < data.length; i++) { #>
            <li class="products__row-item">
				<div class="product-blank">
					<div class="product-blank__head">
						<a href="{{ data[i].permalink }}" class="product-blank__img">
							<img  class="object-fit is--contain" data-object-fit="contain" src="{{ data[i].img }}">
						</a>
					</div>
					<div class="product-blank__body">
						<div class="product-blank__name">
							<h3>{{ data[i].title }}</h3>
						</div>
						<div class="product-blank__cost">
							<p><span>{{ data[i].price }}</span> грн</p>
						</div>
					</div>
					<div class="product-blank__footer">
						<a href="{{ data[i].permalink }}" class="btn">Купить</a>
					</div>
				</div>
			</li>
            <# } #>
		</script>
		<a id="load-more" href="javascript:void(0);" class="btn btn--secondary">Загрузить еще</a>
	</div>
</section>
<?php }; ?>