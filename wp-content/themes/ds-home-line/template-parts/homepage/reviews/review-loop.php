<?php 
defined( 'ABSPATH' ) || exit;
if ( function_exists('carbon_get_post_meta') ) {
    
    $associations = carbon_get_post_meta(get_the_ID(), 'rw_association');

    if ( !empty($associations) ) {
        
        query_posts(array(
            'posts_per_page' => -1,
            'post_type' => 'review',
            'post__in' => getIDsFromAssociation($associations)
        ));
?>
    <div class="reviews-wrap slick-carousel">
    	<?php 
    	if ( have_posts() ) {
    	    while ( have_posts() ) { the_post();
    	        get_template_part('template-parts/homepage/reviews/review', 'single'); 
    	    }
    	}
    	?>	
    </div>
<?php 
       wp_reset_postdata();
       wp_reset_query();
    }
}
?>