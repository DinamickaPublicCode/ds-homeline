<?php 
defined( 'ABSPATH' ) || exit;
$display_type = carbon_get_post_meta(get_the_ID(), 'display_type');
?>
<?php if ($display_type == 'text') { ?>

    <div class="reviews">
    	<div class="reviews__blank">
    		<div class="reviews__blank-top">
    			<div class="reviews__blank-img">
    				<?php 
    				the_post_thumbnail('thumbnail', array( 
    				    'class' => 'object-fit is--cover',
    				    'data-object-fit' => 'cover'
    				) );
    				?>
    			</div>
    			<div class="reviews__blank-info">
    				<p><strong><?php the_title(); ?></strong></p>
    				<p><?php echo carbon_get_post_meta(get_the_ID(), 'instagram'); ?></p>
    				<div class="reviews__blank-icon">
    					<img src="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/socials/instagram-dark.svg" alt="instagram">
    				</div>
    			</div>
    		</div>
    		<div class="reviews__blank-bottom">
    			<?php the_content(); ?>
    		</div>
    	</div>
    </div>
    
<?php } else if ($display_type == 'photo') { ?>

    <div class="reviews">
    	<div class="reviews__blank reviews__blank--secondary">
    		<div class="reviews__blank-top">
    			<div class="reviews__blank-img">
    				<?php
    				$photo = carbon_get_post_meta(get_the_ID(), 'photo');
    				if ( !empty($photo) ) {
    				    echo wp_get_attachment_image( $photo, 'large', false, array(
    				        'class' => 'object-fit is--cover',
    				        'data-object-fit' => 'cover'
    				    ));
    				}
    				?>
    			</div>
    			<div class="reviews__blank-info">
    				<div class="reviews__blank-img is--avatar">
    				<?php 
    				the_post_thumbnail('thumbnail', array( 
    				    'class' => 'object-fit is--cover',
    				    'data-object-fit' => 'cover'
    				) );
    				?>
    				</div>
    				<p><strong><?php the_title(); ?></strong></p>
    				<p><?php echo carbon_get_post_meta(get_the_ID(), 'instagram'); ?></p>
    				<div class="reviews__blank-icon">
    					<img src="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/socials/instagram-dark.svg" alt="instagram">
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    
<?php } ?>