<?php defined( 'ABSPATH' ) || exit; ?>
<?php if ( function_exists('carbon_get_post_meta') ) { ?>
<div class="layout__title">
	<h2><?php echo carbon_get_post_meta(get_the_ID(), 'rw_title'); ?></h2>
</div>
<?php } ?>