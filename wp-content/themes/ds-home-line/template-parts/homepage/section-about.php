<?php 
defined( 'ABSPATH' ) || exit;
?>
<section class="layout__sect">

	<div class="container">
	
		<div class="container__contains">
		
			<?php get_template_part('template-parts/homepage/about/title'); ?>
			
			<div class="layout__info">
			
				<?php get_template_part('template-parts/homepage/about/description'); ?>
				
			</div>
			
			<?php get_template_part('template-parts/homepage/about/advantages'); ?>
			
		</div>
		
	</div>
	
</section>