(function($){
	
	"use strict";
	
	// Declare variation (add-to-cart) form
	var form = $('#single-variation-form');
	
	var variations = {
		// Product variations data -> JSON
		data : form.data('product_variations'),
		// Price block selector
		price : $('.product-blank__cost'), //$('#variation-price')
		// Attribut prefix -> WooCommerce 
		attrPrefix : 'attribute_',
		paPrefix : 'pa_',
		fullPrefix : 'attribute_pa_',
		//Set default data on page depends on first variation that comming
		init : function () {
			this.setDefaultPrice();
		},
		
		/**
		 * Set default price for variation product dependes on first 
		 * variation -> this.data 
		 */
		setDefaultPrice : function () {
			// Retrieve first product variation
			let variation = this.data[0];
			// If we have size hash in url need to get first variation with this value
			if (window.location.hash) {
				let hash = window.location.hash.substring(1);
				let stop = false;
				this.data.forEach(function(varsh, i){
					if (!stop) {
						 for (var option in varsh.attributes) {
							if (varsh.attributes[option] == hash) {
								variation = varsh;
								stop = true;
								break;
							};
						};
					};
				});
			};
			
			this.setVariation(variation);
		},

		setVariation : function (variation) {
			if ( variation.is_in_stock === false ) {
				hideShowAddToCart(true);
				this.price.html(variation.availability_html);
			} else {
				this.price.html(variation.price_html);
			};
			
			// Checking default radio buttons
			for (var option in variation.attributes) {
				let group = $(option.replace(this.attrPrefix, '#'));
				let radio = group.find('input#' + variation.attributes[option]);
				radio.click();
				// radio.attr('checked', 'checked');
			};
			
			setVariationID(variation.variation_id);
		},

		setByImageID : function (imageID) {
			$.each(this.data, function(i, variation){
				if (variation.image_id == imageID) {
					let hideBy = variation.attributes[variations.fullPrefix + variations.hideBy];
					if (hideBy == undefined) {
						variations.setVariation(variation);
					} else {
						console.log(variation);
						variations.hideColors(hideBy, variation);
					}
					return false;
				};
			});
		},

		hideBy : productAttrHideValues.by,
		hideAttr : productAttrHideValues.color,
		hideColors : function (type, customVariation) {
			let findedFirstVariation = false;
			$.each(this.data, function(i, variation) {
				let curType = variation.attributes[variations.fullPrefix + variations.hideBy];
				if (curType != type) {
					$('input#' + variation.attributes[variations.fullPrefix + variations.hideAttr]).parent('li').hide();
				} else {
					$('input#' + variation.attributes[variations.fullPrefix + variations.hideAttr]).parent('li').show();
					if (!findedFirstVariation) {
						findedFirstVariation = true;
						if (customVariation != undefined) {
							variations.setVariation(customVariation);
						} else {
							variations.setVariation(variation);
						}
					}
				}
			});
			
		}

	};

	var firstTimeTriggerHideColor = false;
	$('[name=' + variations.paPrefix + variations.hideBy + ']').on('click', function (event) {
		if (!firstTimeTriggerHideColor) {
			firstTimeTriggerHideColor = true;
		} else if (event.isTrigger) {
			return false;
		}
		variations.hideColors($(this).attr('id'));
	});

	// Init first variation data on view
	variations.init();
	
	$('[data-type="options-list"] input[type="radio"]').on('click', function(){
		// Current clicked radio button
		let radio = $(this);
		// Id of list for currently clicked radio button
		let attr = radio.parents('ul#' + radio.attr('name')).attr('id');
		// Full name of ID that exists in single variation object
		let fullAttr = variations.attrPrefix + attr;
		
		// Array|container of available variations
		// regrading to currently clicked option
		let available = [];
		
		// Retrieve available variations regrading to current clicked option
		variations.data.forEach(function(variation, i){
			if (variation.attributes[fullAttr] === radio.val()) {
				available.push(variation);
			};
		});
		
		// Getting currently selected attributes
		let selected = {};
		$('[data-type="options-list"] input[type="radio"]:checked').each(function(){
			selected[variations.attrPrefix + $(this).attr('name')] = $(this).attr('id');
		});
		
		// Define final result variation
		let resultVariation;
		// Check all variation for matches
		available.forEach(function(variation, index, array){
			let trigger = 0;
			// Check each attribute value for matching with selected values
			for (var option in variation.attributes) {
				if (selected[option] === variation.attributes[option]) {
					trigger++;
				}
			};
			// trigger nubmer == size of selected means we find correct variation and break the loop
			if (trigger == _.size(selected)) {
				resultVariation = variation;
				return true;
			}
		});
		
		if (resultVariation === undefined)  {
//			variations.price.html(wc_add_to_cart_variation_params.i18n_no_matching_variations_text);
			variations.price.html('Товар отсутствует');
			hideShowAddToCart(true);
		} else {
//			variations.price.html(resultVariation.availability_html + resultVariation.price_html);
			// Show different text/price depends on product availability
			if ( resultVariation.is_in_stock === false ) {
				hideShowAddToCart(true);
				variations.price.html(resultVariation.availability_html);
			} else {
				hideShowAddToCart();
				variations.price.html(resultVariation.price_html);
				setVariationID(resultVariation.variation_id);
			};
			
			changeSliderImage(resultVariation.image_id);// Works only for variaton product with images in variations
		}

	});
	
	// Hide show AddToCart button if need
	function hideShowAddToCart(hide) {
		if (hide) {
			form.find('button[type="submit"]').hide();
			form.find('a.buy-on-fly').hide();
		} else {
			form.find('button[type="submit"]').show();
			form.find('a.buy-on-fly').show();
		}
	}
	
	// Set currently choosen variation ID
	function setVariationID(id) {
		$('input[type="hidden"][name="variation_id"]').val(id);
	}
	
	// Change slider image to currenly selected variation
	// Works only for variaton product with images in variations
	function changeSliderImage(imageID) {
		let slideNav = $('.detail-card__nav[data-slider-type="variation"]');
		if (slideNav.length == 0) {
			return false
		}
		let slide = slideNav.find('.detail-card__item[data-variation-slide="'+ imageID +'"]:not(".slick-cloned")');
		slide.click();
	}

	$(document).on('click', '.detail-card__item[data-variation-slide]', function(event){
		if (event.isTrigger) {
			return false;
		}
		variations.setByImageID($(this).data('variation-slide'));
	}); 
	
	// Custom Product variation form submit event
	// Disable form sending
	form.submit(function(event){
		event.preventDefault();
		let data = {
			action: 'dhl_ajax_add_to_cart',
		};
		
		$.each(form.serializeArray(), function(i, field){
			data[field.name] = field.value;
		});
		
		customAddToCart(data);
		
		setTimeout(function(){
			$(".header__item #cart").click();
		}, 200)
	});

	$(document).on('click', '#crosssell-add-to-cart', function(){
		let data = {
			action: 'dhl_ajax_add_to_cart',
		};

		$.each($(this).data(), function(name, value){
			data[name] = value;
		});
		customAddToCart(data);
	});
	
	function customAddToCart(data) {
		$.ajax({
            type: 'post',
            url: ajaxData.url,
            data: data,
            beforeSend: function (response) {
            	
            },
            complete: function (response) {
            	
            },
            success: function (response) {
            	$('div.mini-cart-ajax').replaceWith(response.fragments['div.mini-cart-ajax']);
            	$('div.mini-cart-data').replaceWith(response.fragments['div.mini-cart-data']);
            },
        });
	}

	function update_product_description(){
		var id = $('.variation_id').val();
		var product_variation = $("#single-variation-form").data('product_variations').filter(product_var => product_var.variation_id == id )[0];
		$('#variation_description').html(product_variation.variation_description);
	}

	update_product_description();

	$( document ).on('click', "#single-variation-form input[type=radio]", function () {
		update_product_description();
	} );
	
})(jQuery);

