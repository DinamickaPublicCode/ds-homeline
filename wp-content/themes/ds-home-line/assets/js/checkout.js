(function($){
	
	"use strict";
	
	var checkoutButton = $('#checkout-reg-btn');
	checkoutButton.data('type', 1);
	
	var regBlock = $('div[data-tab-content="1"]');
	var logBlock = $('div[data-tab-content="2"]');
	
	$('form#checout-registration a[data-tab]').on('click', function(event){
		checkoutButton.data('type', $(this).data('tab'));
	});
	
	checkoutButton.on('click', function(event){
		event.preventDefault();
		if ($(this).data('type') == 1) {
				registerNewCustomer();
		} else if ($(this).data('type') == 2) {
			signInCustomer();
		}
	});
	
	function registerNewCustomer() {
		let data = {};
		regBlock.find('input[name]').each(function(){
			if ($(this).prop('name') == "subscribe" && !$(this).prop('checked')) {
				return;
			}
			data[$(this).prop('name')] = $(this).val();
		});

		var validate = false,
				currentForm = $("#checout-registration"),
				currentFormActive = currentForm.find(".form__step-contains.is--active"),
				inputUserRegEmail = currentFormActive.find("[name=email]"),
				inputUserName = currentFormActive.find("[name=first_name]"),
				inputUserLastName = currentFormActive.find("[name=last_name]"),
				inputUserPhone = currentFormActive.find("[name=billing_phone]");


		$("#checout-registration .form__step.is--active--step .form__step-contains.is--active ").find(".woocommerce-error").remove();

		/************Валидация поля inputUserPhone*************/
		if (inputUserPhone.val() !== "") {
			inputUserPhone.siblings("p").remove();
			inputUserPhone.removeClass("is--error");
			if (inputUserPhone.val().length == 19) {
				inputUserPhone.siblings("p").remove();
				inputUserPhone.removeClass("is--error");
			} else {
				inputUserPhone.addClass("is--error");
				inputUserPhone.after("<p>Не верный формат.</p>");
			}
		} else {
			inputUserPhone.siblings("p").remove();
			inputUserPhone.addClass("is--error");
			inputUserPhone.after("<p>Поле обязательно для заполнения.</p>");
		}
		/************конец Валидации поля inputUserPhone*************/

		/************Валидация поля inputUserLastName*************/
		if (inputUserLastName.val() !== "") {
			inputUserLastName.siblings("p").remove();
			inputUserLastName.removeClass("is--error");
		} else {
			inputUserLastName.siblings("p").remove();
			inputUserLastName.addClass("is--error");
			inputUserLastName.after("<p>Поле обязательно для заполнения.</p>");
		}
		/************конец Валидации поля inputUserLastName*************/

		/************Валидация поля inputUserName*************/
		if (inputUserName.val() !== "") {
			inputUserName.siblings("p").remove();
			inputUserName.removeClass("is--error");
		} else {
			inputUserName.siblings("p").remove();
			inputUserName.addClass("is--error");
			inputUserName.after("<p>Поле обязательно для заполнения.</p>");
		}
		/************конец Валидации поля inputUserName*************/

		/************Валидация поля E-mail*************/
		if (inputUserRegEmail.val() !== "") {
			var patternEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
			if (patternEmail.test(inputUserRegEmail.val())) {
				inputUserRegEmail.removeClass("is--error");
				inputUserRegEmail.siblings("p").remove();
			} else {
				inputUserRegEmail.siblings("p").remove();
				inputUserRegEmail.addClass("is--error");
				inputUserRegEmail.after("<p>Не верный формат.</p>");
			}

		} else {
			inputUserRegEmail.siblings("p").remove();
			inputUserRegEmail.addClass("is--error");
			inputUserRegEmail.after("<p>Поле обязательно для заполнения.</p>");
		}
		/************конец Валидации поля E-mail*************/

		if (currentFormActive.find(".form__item input").hasClass("is--error")) {
			validate = false;
		} else {
			validate = true;
		}

		if (validate === true) {
			$.ajax( {
				url: ajaxData.url,
				type: 'POST',
				data: {action: 'dhl_checkout_registration', data: data},
				success: function ( response ) {
				console.log(response);
					$("#checout-registration .form__step.is--active--step .form__step-contains.is--active ").find(".woocommerce-error").remove();
					$("#checout-registration .form__step.is--active--step .form__step-contains.is--active ").prepend((response.data.submessage == 'Извините, это имя пользователя уже существует!') ? '<div class="woocommerce-error">' + response.data.submessage + '</div>' : '');
					if ( response.success ) {
						setTimeout( function () {
							location.reload();
						}, 1000 );
					}
				},
			} );
		}
	}
	
	function signInCustomer() {
		let data = {};
		logBlock.find('input[name]').each(function(){
			data[$(this).prop('name')] = $(this).val();
		});

		var validate = false,
			currentForm = $("#checout-registration"),
			currentFormActive = currentForm.find(".form__step-contains.is--active"),
			inputUserRegEmail = currentFormActive.find("[name=email]"),
			inputUserPassword = currentFormActive.find("[name=password]");

		$("#checout-registration .form__step.is--active--step .form__step-contains.is--active ").find(".woocommerce-error").remove();

		/************Валидация поля E-mail*************/
		if (inputUserRegEmail.val() !== "") {
			var patternEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
			if (patternEmail.test(inputUserRegEmail.val())) {
				inputUserRegEmail.removeClass("is--error");
				inputUserRegEmail.siblings("p").remove();
			} else {
				inputUserRegEmail.siblings("p").remove();
				inputUserRegEmail.addClass("is--error");
				inputUserRegEmail.after("<p>Не верный формат.</p>");
			}

		} else {
			inputUserRegEmail.siblings("p").remove();
			inputUserRegEmail.addClass("is--error");
			inputUserRegEmail.after("<p>Поле обязательно для заполнения.</p>");
		}
		/************конец Валидации поля E-mail*************/

		/************Валидация поля password*************/
		if (inputUserPassword.val() !== "") {
			inputUserPassword.siblings("p").remove();
			inputUserPassword.removeClass("is--error");
		} else {
			inputUserPassword.siblings("p").remove();
			inputUserPassword.addClass("is--error");
			inputUserPassword.after("<p>Поле обязательно для заполнения.</p>");
		}

		inputUserPassword.on("change", function () {
			$(this).siblings("p").remove();
		});
		/************конец Валидации поля password*************/

		if (currentFormActive.find(".form__item input").hasClass("is--error")) {
			validate = false;
		} else {
			validate = true;
		}

		if (validate === true) {
			$.ajax( {
				url: ajaxData.url,
				type: 'POST',
				data: {action: 'dhl_checkout_login', data: data},
				success: function ( response ) {
					console.log(response.data.submessage);
					$("#checout-registration .form__step.is--active--step .form__step-contains.is--active ").find(".woocommerce-error").remove();
					$("#checout-registration .form__step.is--active--step .form__step-contains.is--active ").prepend((response.data.submessage == '<strong>ОШИБКА</strong>: Неверный адрес e-mail. <a href="http://ds-home-line/my-account/lost-password/">Забыли пароль?</a>') ? '<div class="woocommerce-error">' + response.data.submessage + '</div>' : '');
					if ( response.success ) {
						setTimeout( function () {
							location.reload();
						}, 3000 );
					}
				},
			} );
		}
	}
	
	
	$('#chekout-form').on('submit', function(event) {
		event.preventDefault();
		
		let dataContainer = $(this).find('.form__step-contains.is--active');
		let data = {};
		
		data['shipping_company'] = dataContainer.find('input[type="submit"]').data('type');
		
		dataContainer.find('input:not([type="submit"]), select').each(function(){
			
			if ($(this).prop('name') == "shipping_rate_method") {
				let option = $(this).find('option:selected');
				data[$(this).prop('name')] = {
					shipping_id : option.data('index'),
					shipping_method : option.data('method_id'),
					instance_id : option.val(),
					shipping_name : option.text()
				}
			} else {
				data[$(this).prop('name')] = $(this).val();
			}
		});

		var validate = false,
			currentForm = $("#chekout-form"),
			currentFormActive = currentForm.find(".form__step-contains.is--active"),
			inputUserName = currentFormActive.find("[name=shipping_first_name]"),
			inputUserCity = currentFormActive.find("[name=shipping_city]"),		
			inputUserDepartment = currentFormActive.find("[name=shipping_department]"),
			inputUserLastName = currentFormActive.find("[name=shipping_last_name]"),
			inputUserPhone = currentFormActive.find("[name=billing_phone]"),
			inputUserDepartment2 = currentFormActive.find("[name=shipping_address_1]");


		$("#checout-registration .form__step.is--active--step .form__step-contains.is--active ").find(".woocommerce-error").remove();

		/************Валидация поля inputUserDepartment2*************/
		if (inputUserDepartment2.val() !== "") {
			inputUserDepartment2.siblings("p").remove();
			inputUserDepartment2.removeClass("is--error");
		} else {
			inputUserDepartment2.siblings("p").remove();
			inputUserDepartment2.addClass("is--error");
			inputUserDepartment2.after("<p>Поле обязательно для заполнения.</p>");
		}
		/************конец Валидации поля inputUserDepartment2*************/

		/************Валидация поля inputUserDepartment*************/
		if (inputUserDepartment.val() !== "") {
			inputUserDepartment.siblings("p").remove();
			inputUserDepartment.removeClass("is--error");
		} else {
			inputUserDepartment.siblings("p").remove();
			inputUserDepartment.addClass("is--error");
			inputUserDepartment.after("<p>Поле обязательно для заполнения.</p>");
		}
		/************конец Валидации поля inputUserDepartment*************/

		/************Валидация поля inputUserCity*************/
		if (inputUserCity.val() !== "") {
			inputUserCity.siblings("p").remove();
			inputUserCity.removeClass("is--error");
		} else {
			inputUserCity.siblings("p").remove();
			inputUserCity.addClass("is--error");
			inputUserCity.after("<p>Поле обязательно для заполнения.</p>");
		}
		/************конец Валидации поля inputUserCity*************/

		/************Валидация поля inputUserPhone*************/
		if (inputUserPhone.val() !== "") {
			inputUserPhone.siblings("p").remove();
			inputUserPhone.removeClass("is--error");
			if (inputUserPhone.val().length == 19) {
				inputUserPhone.siblings("p").remove();
				inputUserPhone.removeClass("is--error");
			} else {
				inputUserPhone.addClass("is--error");
				inputUserPhone.after("<p>Не верный формат.</p>");
			}
		} else {
			inputUserPhone.siblings("p").remove();
			inputUserPhone.addClass("is--error");
			inputUserPhone.after("<p>Поле обязательно для заполнения.</p>");
		}
		/************конец Валидации поля inputUserPhone*************/

		/************Валидация поля inputUserLastName*************/
		if (inputUserLastName.val() !== "") {
			inputUserLastName.siblings("p").remove();
			inputUserLastName.removeClass("is--error");
		} else {
			inputUserLastName.siblings("p").remove();
			inputUserLastName.addClass("is--error");
			inputUserLastName.after("<p>Поле обязательно для заполнения.</p>");
		}
		/************конец Валидации поля inputUserLastName*************/

		/************Валидация поля inputUserName*************/
		if (inputUserName.val() !== "") {
			inputUserName.siblings("p").remove();
			inputUserName.removeClass("is--error");
		} else {
			inputUserName.siblings("p").remove();
			inputUserName.addClass("is--error");
			inputUserName.after("<p>Поле обязательно для заполнения.</p>");
		}
		/************конец Валидации поля inputUserName*************/
		
		if (currentFormActive.find(".form__item input").hasClass("is--error")) {
			validate = false;
		} else {
			validate = true;
		}

		if (validate === true) {
			$.ajax( {
				url: ajaxData.url,
				type: 'POST',
				data: {action: 'dhl_create_order', data: data},
				success: function ( response ) {
					if (response.success) {
						// displaySuccessCheckoutModal();
						// setTimeout(function(){
							location.href = response.data.href;
						// }, 2000);
					}
				},
			} );
		}
		return false;
	});
	
	function displaySuccessCheckoutModal() {
		hideScroll();
		$('.pop-ups').addClass('is--active is--fade');
		$('.pop__up[data-name-popup="success-order"]').addClass('is--show');
	}
	
})(jQuery);