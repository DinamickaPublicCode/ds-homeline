(function ($) {

	"use strict";

	function displayModalPopup(selector) {
		$('.pop-ups').addClass('is--active is--fade');
		$('.pop__up' + selector).addClass("is--show--secondary");
		setTimeout(function() {
			$('.pop__up' + selector).addClass("is--show--fade");
		}, 500)
	}

	$(document).on('click', '.buy-on-fly', function (e) {
		e.preventDefault();

		$(".pop-ups").find(".is--show").removeClass("is--show");

		displayModalPopup('[data-name-popup="form-order"]');

	});


	$(document).on('change', '.counter input[type="number"]', function () {
		var input = $(this);
		$.ajax({
			type: 'post',
			url: ajaxData.url,
			data: {
				action: 'product_quantity_update',
				quantity: input.val(),
				productKey: input.data('cart_item_key')
			},
			success: function (response) {
				$('div.mini-cart-ajax').replaceWith(response.fragments['div.mini-cart-ajax']);
				$('div.mini-cart-data').replaceWith(response.fragments['div.mini-cart-data']);
				$('section.full-cart').replaceWith(response.fragments['section.full-cart']);
			},
		});
	});
	

	$(document).on('keypress', '.counter input[type="number"]', function (e) {
		var input = $(this);

		if (e.which == "13") {
			$.ajax({
				type: 'post',
				url: ajaxData.url,
				data: {
					action: 'product_quantity_update',
					quantity: input.val(),
					productKey: input.data('cart_item_key')
				},
				success: function (response) {
					$('div.mini-cart-ajax').replaceWith(response.fragments['div.mini-cart-ajax']);
					$('div.mini-cart-data').replaceWith(response.fragments['div.mini-cart-data']);
					$('section.full-cart').replaceWith(response.fragments['section.full-cart']);
				},
			});
		}
	});
})(jQuery);