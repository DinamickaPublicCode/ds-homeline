(function($){
	
	"use strict";
	
	// Load size data template by given data
	function loadSizeData(size) {
		let tmpl = wp.template(sizes.template_id);
		sizes.template.html(tmpl(size));
	}
	
	
	// Define size data object
	let sizes = {
		elementsCount : 4,
		elements : $('.category__link[data-slug]'),
		template : $('#sizes-template'),
		template_id : 'sizes-template',
		attr_json : 'attributes',
		activeClass : 'is--active',
		removeActiveClasses : function () {
			this.elements.removeClass(this.activeClass);
		},
		getAllSizes : function () {
			return this.template.data(sizes.attr_json);
		},
		init : function () {
			let first = this.template.data(this.attr_json)[0];
			loadSizeData(first);
			products.getProductsBySize(first.slug);
		},
		selectCurrentSize : function (element, increaseElementCount) { // return selected size slug
			
			if (increaseElementCount == true) {
				this.elementsCount = this.elementsCount + 4;
			}
			
			let sizeArr = this.getAllSizes();
			for (var index in sizeArr) {
				let size = sizeArr[index];
				if (size.slug == element.data('slug')) {
					this.removeActiveClasses();
					element.addClass(this.activeClass);
					loadSizeData(size);
					products.getProductsBySize(size.slug, this.elementsCount);
					break;
				}
			}
		},
		refreshProductsCount: function() {
			this.elementsCount = 4;
		}
	}
	
	let products = {
		template : $('#sizes-products'),
		template_id : 'sizes-products',
		getProductsBySize: function (size, elementsCount) {
			$.ajax({
				url : ajaxData.url,
				method : 'post',
				data : {'action': 'products_by_size', 'slug': size, 'elementsCount' : elementsCount},
				success : function (response) {
					if ( response.products.length != 0 ) {
						products.renderProducts(response.products);
					}
					if (!response.show_load_more) {
						$('#load-more').hide();
					} else {
						$('#load-more').show();
					}
				}
			});
		},
		renderProducts : function (data) {
			let tmpl = wp.template(this.template_id);
			this.template.html(tmpl(data));
		}
		
	}
	// Load first size data
	sizes.init();
	
	sizes.elements.on('click', function(){
		let size = sizes.selectCurrentSize($(this));

		/********************tablet version*******************/
		var currentElementText = $(this).text(),
			parents = $(this).parents(".category__list"),
			categoryList = parents.siblings(".category__dropdown");
			
			categoryList.find(".category__dropdown-text").text(currentElementText);

		if ( $(window).width() <=1199 ) {
			parents.slideUp();
		}

	});
	
	$('#load-more').on('click', function(){
		let size = sizes.selectCurrentSize($('.category__link.is--active[data-slug]'), true);

		/********************tablet version*******************/
		var currentElementText = $(this).text(),
			parents = $(this).parents(".category__list"),
			categoryList = parents.siblings(".category__dropdown");
			
			categoryList.find(".category__dropdown-text").text(currentElementText);

		if ( $(window).width() <=1199 ) {
			parents.slideUp();
		}

	});
	
	
})(jQuery);