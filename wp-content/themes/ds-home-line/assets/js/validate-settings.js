function validateForm(form) {
    var currentForm = form,
        save = currentForm.find("[type=submit]"),
        inputUserEmail = currentForm.find("[name=username]"),
        inputUserRegEmail = currentForm.find("[name=email]"),
        inputUserPassword = currentForm.find("[name=password]"),
        inputUserName = currentForm.find("[name=first_name]"),
        inputUserLastName = currentForm.find("[name=last_name]"),
        inputUserPhone = currentForm.find("[name=billing_phone]");

    save.on("click", function () {

        console.log("test");

        if (currentForm.hasClass("is--valid")) {
            return true;
        } else {

            /************Валидация поля inputUserPhone*************/
            if (inputUserPhone.val() !== "" && inputUserPhone.length) {
                inputUserPhone.siblings("p").remove();
                inputUserPhone.removeClass("is--error");
                if (inputUserPhone.val().length == 19) {
                    inputUserPhone.siblings("p").remove();
                    inputUserPhone.removeClass("is--error");
                } else {
                    inputUserPhone.addClass("is--error");
                    inputUserPhone.after("<p>Не верный формат.</p>");
                }
            } else {
                inputUserPhone.siblings("p").remove();
                inputUserPhone.addClass("is--error");
                inputUserPhone.after("<p>Поле обязательно для заполнения.</p>");
            }
            /************конец Валидации поля inputUserPhone*************/

            /************Валидация поля inputUserLastName*************/
            if (inputUserLastName.val() !== "" && inputUserLastName.length) {
                inputUserLastName.siblings("p").remove();
                inputUserLastName.removeClass("is--error");
            } else {
                inputUserLastName.siblings("p").remove();
                inputUserLastName.addClass("is--error");
                inputUserLastName.after("<p>Поле обязательно для заполнения.</p>");
            }
            /************конец Валидации поля inputUserLastName*************/

            /************Валидация поля inputUserName*************/
            if (inputUserName.val() !== "" && inputUserName.length) {
                inputUserName.siblings("p").remove();
                inputUserName.removeClass("is--error");
            } else {
                inputUserName.siblings("p").remove();
                inputUserName.addClass("is--error");
                inputUserName.after("<p>Поле обязательно для заполнения.</p>");
            }
            /************конец Валидации поля inputUserName*************/

            /************Валидация поля E-mail*************/
            if (inputUserEmail.val() !== "" && inputUserEmail.length) {
                var patternEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
                if (patternEmail.test(inputUserEmail.val())) {
                    inputUserEmail.removeClass("is--error");
                    inputUserEmail.siblings("p").remove();
                } else {
                    inputUserEmail.siblings("p").remove();
                    inputUserEmail.after("<p>Не верный формат.</p>");
                }

            } else {
                inputUserEmail.siblings("p").remove();
                inputUserEmail.addClass("is--error");
                inputUserEmail.after("<p>Поле обязательно для заполнения.</p>");
            }
            /************конец Валидации поля E-mail*************/

            /************Валидация поля E-mail*************/
            if (inputUserRegEmail.val() !== "" && inputUserRegEmail.length) {
                var patternEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
                if (patternEmail.test(inputUserRegEmail.val())) {
                    inputUserRegEmail.removeClass("is--error");
                    inputUserRegEmail.siblings("p").remove();
                } else {
                    inputUserRegEmail.siblings("p").remove();
                    inputUserRegEmail.after("<p>Не верный формат.</p>");
                }

            } else {
                inputUserRegEmail.siblings("p").remove();
                inputUserRegEmail.addClass("is--error");
                inputUserRegEmail.after("<p>Поле обязательно для заполнения.</p>");
            }
            /************конец Валидации поля E-mail*************/


            /************Валидация поля password*************/
            if (inputUserPassword.val() !== "" && inputUserPassword.length) {
                inputUserPassword.siblings("p").remove();
                inputUserPassword.removeClass("is--error");
            } else {
                inputUserPassword.siblings("p").remove();
                inputUserPassword.addClass("is--error");
                inputUserPassword.after("<p>Поле обязательно для заполнения.</p>");
            }

            inputUserPassword.on("change", function () {
                $(this).siblings("p").remove();
            });
            /************конец Валидации поля password*************/


            /************Проверка нет ли ошибок в полях*************/
            if (currentForm.find(".form__item input").hasClass("is--error")) {
                currentForm.removeClass("is--valid");
            } else {
                currentForm.addClass("is--valid");
                save.click();
            }
            /************конец проверки нет ли ошибок в полях*************/

            return false;
        }
    });
}

$(document).ready(function () {
    validateForm($(".woocommerce-form-login"));
    validateForm($(".woocommerce-form-register"));
});