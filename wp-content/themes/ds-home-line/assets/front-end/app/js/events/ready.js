var body = document.body,
	currentPos = 0;

function hideScroll() {
	var intElemScrollTop = parseInt(window.pageYOffset),
		topElem = body.style.top;
	currentPos = intElemScrollTop;
	body.classList.add('hide--scroll');
	body.style.position = 'fixed';
	topElem = intElemScrollTop * (-1);
	body.style.top = topElem + 'px';
}

function showScroll() {
	body.classList.remove('hide--scroll');
	body.style.position = '';
	body.style.top = '';
	window.scrollTo(0, currentPos);
}

// function initHeight() {
// 	var element = $(".overview-wrap.slick-carousel"),
// 		elementText = element.find(".slick-current").find(".overview__blank-text"),
// 		sliderDots = element.find(".slick-dots"),
// 		offset = elementText.offset().top - elementText.parents(element).offset().top;

// 	sliderDots.css("top", (elementText.outerHeight() + offset - 30) + "px");

// 	setTimeout(function () {
// 		sliderDots.css("opacity", "1");
// 	}, 500)
// }


function mask(event) {
	var matrix = "+38 (0__) ___-__-__",
		i = 0,
		def = matrix.replace(/\D/g, ""),
		val = this.value.replace(/\D/g, "");
	if (def.length >= val.length) val = def;
	this.value = matrix.replace(/./g, function (a) {
		return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
	});
	if (event.type == "blur") {
		if (this.value.length == 6) {
			this.value = "";
		}
	}

	if (event.type == "click") {
		var element = this;
		if (getCaretPosition(element) == 0 || getCaretPosition(element) == 1 || getCaretPosition(element) == 2 || getCaretPosition(element) == 3) {
			setCaretPosition(element, 4);
		}
	}

	if (event.type == "input") {
		var element = this;

		if (getCaretPosition(element) == 0 || getCaretPosition(element) == 1 || getCaretPosition(element) == 2 || getCaretPosition(element) == 3) {
			setCaretPosition(element, 4);
		}
	}
};

function getCaretPosition(element) {
	var caretPos = 0;
	if (element.type === 'tel') {
		if (document.selection) { // IE Support
			element.focus();
			var Sel = document.selection.createRange();
			Sel.moveStart('character', -element.value.length);
			caretPos = Sel.text.length;
		} else if (element.selectionStart || element.selectionStart === '0') { // Firefox support
			caretPos = element.selectionStart;
		}
	}
	return caretPos;
}

function setCaretPosition(element, position) {
	if (element.type === 'tel') {
		if (element.setSelectionRange) {
			element.focus();
			element.setSelectionRange(position, position);
		} else if (element.createTextRange) {
			var range = element.createTextRange();
			range.collapse(true);
			range.moveEnd('character', position);
			range.moveStart('character', position);
			range.select();
		}
	}
}

function fixedColorPallete(){
	var row = $(".layout__static-row"),
		fixedRow = $(".layout__wrap");
	row.css("height", fixedRow.outerHeight() + "px");

	if (fixedRow.length && row.length ) {

		if ( $(window).scrollTop() + $(window).height() > row.offset().top + row.outerHeight() ) {
			fixedRow.addClass("active");
		} else {
			fixedRow.removeClass("active");
		}
	}


	// if ($(document).scrollTop() + $(window).height() > $(row).offset().top && $(document).scrollTop() - $(row).offset().top < $(row).height()) {
	// 	console.log("ВИДЕН")
	// } else {
	// 	console.log("НЕ ВИДЕН")
	// }
}

$(document).ready(function () {
	var carousel = $(".slick-carousel"),
		productSet = $("#product-set"),
		productIMG = $("#product-img"),
		productPrice = $("#product-price"),
		productLink = $("#product-link"),
		sandwich = $("#sandwich"),
		nav = $("#navigation"),
		subMenu = $("[data-menu]"),
		menuItem = $(".nav__item.is--submenu--second"),
		tabs = $("[data-tabs]"),
		tab = $("[data-tab]"),
		btnList = $("[data-list-btn] .btn"),
		popups = $(".pop-ups"),
		closePopup = $(".close"),
		profile = $("#profile"),
		profileBlank = $("#profile-blank"),
		inputPhone = $("input[type='tel']"),
		selectClone = $("[data-select]"),
		selectCloneOption = $(".select-clone__option"),
		selectWrap = $("[data-select-wrap]"),
		dataStepNext = $("[data-step-next]"),
		dataStepPrev = $("[data-step-prev]"),
		categoryDropdown = $(".category__dropdown");

	categoryDropdown.on("click",function(){
		if ( $(window).width() <= 1199 ) {
			$(this).siblings(".category__list").slideToggle();
		}
	})


	if ( $(".cabinet").length ) {
		var active = $(".cabinet .cabinet__section.is-active a").text();

		if ( active.length > 1) {
			$(".category__dropdown-text").text(active);
		}

		$(".cabinet .cabinet__section.is-active").hide();
	}

	if ($("body").hasClass("single-product")) {
		$(this).scrollTop(0);
	}


	$(".aside-toggle").on("click", function(e){
		e.preventDefault();

		$(this).toggleClass("is--close");

		$(this).find(".aside-toggle__text #aside-text").text($(this).hasClass("is--close") ? "Показать заказ" : "Спрятать заказ")
	});

	
	/**fixedColorPallete**/
	fixedColorPallete();

	/**data step**/
	dataStepNext.on("click", function (e) {
		e.preventDefault();

		var currentEl = $(this),
				attr = currentEl.attr("data-step-next"),
				wrapper = currentEl.parents("[data-steps]");
		wrapper.find(".is--active--step").removeClass("is--active--step");
		wrapper.find("[data-step=" + attr + "]").addClass("is--active--step");

	});

	dataStepPrev.on("click", function (e) {
		e.preventDefault();

		var currentEl = $(this),
			attr = currentEl.attr("data-step-prev"),
			wrapper = currentEl.parents("[data-steps]");
		wrapper.find(".is--active--step").removeClass("is--active--step");
		wrapper.find("[data-step=" + attr + "]").addClass("is--active--step");

	});

	/**data select (clone)**/
	selectClone.on("click", function(){
		var current = $(this),
			attr = current.attr("data-select"),
			selectOption = current.parents("[data-select-wrap]");
		
		if ( selectOption.hasClass("is--open") ) {
			selectOption.toggleClass("is--open").find("[data-option="+attr+"]").slideToggle("fast");
		} else {
			selectWrap.removeClass("is--open").find("[data-option]").slideUp("fast");
			selectOption.toggleClass("is--open").find("[data-option="+attr+"]").slideToggle("fast");
		}
	})


	selectCloneOption.on("click",function(e){
		e.preventDefault();
		var current = $(this),
			text = current.text(),
			attr = current.attr("value"),
			parentsEl = current.parents("[data-select-wrap]");

		parentsEl.find("option[value=" + attr + "]").prop("selected",true);
		parentsEl.find("[data-select] p").text(text);
		parentsEl.removeClass("is--open").find("[data-option]").slideUp("fast");
	})

	/**Mask phone**/
	inputPhone.on("input click blur", mask);


	/**popin profile**/
	profile.on("click", function (e) {
		e.preventDefault();

		profileBlank.toggleClass("is--active");
	})

	/**pop-ups**/
	$(document).on('click', '[data-get-popup]', function (e) {
		e.preventDefault();
		hideScroll();
		
		var attr = $(this).attr('data-get-popup'),
			currentPopup = popups.find('.pop__up[data-name-popup=' + attr + ']');

		popups.addClass('is--active');

		setTimeout(function () {
			popups.addClass('is--fade');
		}, 100)
		setTimeout(function () {
			currentPopup.addClass('is--show');
		}, 200)
	})

	$(document).on('click', '.close', function () {
		if ( $("body").hasClass("hide--scroll")) {
			showScroll();
		}
		var attr = $(this).attr('data-name-popup'),
			currentPopup = popups.find('.pop__up[data-name-popup=' + attr + ']');
		if (popups.hasClass('is--active')) {

			if ( currentPopup.hasClass("is--show--fade") ) {
				currentPopup.removeClass('is--show--fade');
				
				setTimeout(function() {
			   currentPopup.removeClass('is--show--secondary');
				}, 500)
				
				setTimeout(function () {
					popups.removeClass('is--fade')
				}, 600)
				setTimeout(function () {
					popups.removeClass('is--active')
				}, 700)
											
			} else {
				currentPopup.removeClass('is--show');
				$(".pop-ups").find(".is--show").removeClass("is--show");
				setTimeout(function () {
					popups.removeClass('is--fade')
				}, 200)
				setTimeout(function () {
					popups.removeClass('is--active')
				}, 400)
			}
		}
	});

	// /**counter**/
	// $(document).on("blur", '.counter', function (e) {
	// 	if ($(this).val() == "") {
	// 		$(this).val(1);
	// 	}
	// });

	$(document).on("input keypress", '.counter', function (e) {
		var currentEl = $(this);

		if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
			return false;
		} else if (currentEl.val().length > 3) {
			currentEl.val(currentEl.val().slice(0, -1));
		}
	});

	$(document).on("click", "[data-counter-next]", function () {
		var parents = $(this).parents(".counter"),
				counterInput = parents.find("input"),
				oldValue = parseFloat(counterInput.val()),
				counterMax = counterInput.attr('max');

				if (oldValue >= counterMax) {
					var newVal = oldValue;
				} else {
					var newVal = oldValue + 1;
				}
				
				counterInput.val(newVal);
				counterInput.trigger("change");
	});

	$(document).on("click", "[data-counter-prev]", function () {
		var parents = $(this).parents(".counter"),
			counterInput = parents.find("input"),
			oldValue = parseFloat(counterInput.val()),
			counterMin = counterInput.attr('min');

		if (oldValue <= counterMin) {
			var newVal = oldValue;
		} else {
			var newVal = oldValue - 1;
		}

		counterInput.val(newVal);
		counterInput.trigger("change");
	});


	/**btn (cart, card product):hover**/
	$(document).on("mouseenter", "[data-list-btn] .btn", function () {
		$(this).parents("[data-list-btn]").find(".btn").addClass("btn--fourth");
		$(this).removeClass("btn--fourth");
	});

	/**fancybox**/
	if ($('[data-fancybox]').length > 0) {
		if ( $(window).width() > 767 ) {
			$('[data-fancybox]').fancybox({
				buttons: [
					'close'
				],
				maxWidth: '100%',
				maxHeight: '100%',
				fitToView: !1,
				closeClick: !1,
				openEffect: 'none',
				closeEffect: 'none',
				loop: !1,
			})
		} else {
			$('[data-fancybox]').on("click", function(e) {
				e.preventDefault();
			});

			$.fancybox.destroy();
		}
	}

	/**tabs**/
	tab.on("click", function (e) {
		e.preventDefault();
		var currentTab = $(this),
			parentsTab = currentTab.parents(tabs),
			attr = currentTab.attr("data-tab");

		currentTab.parents(".category").find(".category__dropdown .category__dropdown-text").text(currentTab.text());

		if ($(window).width() <= 1199  && currentTab.hasClass("category__link")) {
			currentTab.parents(".category__list").slideUp();
		}

		if (currentTab.is("[data-tab='login']")) {
			
		} else if (currentTab.is("[data-tab='register']")) {
			parentsTab.find(".woocommerce-notices-wrapper").remove();
			/********просьба beck end разработчика*******/
			 var param = "#register";
			 var url = window.location.pathname;
			 window.history.replaceState(null, null, url + param);
			/********end*******/
		}

		if (currentTab.is("[data-tab='register']")) {

		} else if (currentTab.is("[data-tab='login']")) {
			parentsTab.find(".woocommerce-notices-wrapper").remove();
		}

		parentsTab.find(".is--active").removeClass("is--active");
		currentTab.addClass("is--active");
		parentsTab.find("[data-tab-content=" + attr + "]").addClass("is--active");
	})

	/********просьба beck end разработчика*******/
	if (window.location.hash && window.location.hash == "#register") {
		$("body").find(".layout__info-link[href='#register']").click();
	}
	/********end*******/

	/**color-palette**/
	$(".color-palette__color").on("click", function () {
		$(".color-palette__color").removeClass("is--active");

		var currentEl = $(this),
			dataImg = currentEl.attr("data-image-color"),
			dataPrice = currentEl.attr("data-price"),
			dataSet = currentEl.attr("data-set"),
			dataLink = currentEl.attr("data-href");
		currentEl.addClass("is--active");
		productSet.text(dataSet);
		productIMG.attr("src", dataImg);
		productPrice.text(dataPrice);
		productLink.attr("href", dataLink);
	})

	/**sandwich**/
	sandwich.on("click", function () {
		if (sandwich.hasClass("is--active")) {
			sandwich.removeClass("is--active");
			nav.removeClass("is--active");
			nav.find(".is--active").removeClass("is--active");
			showScroll();
			if ( $(window).width() >= 768 ) {
				sandwich.parents(".header__item").removeClass("header__item-sandwich");
			}
		} else {
			if ($(window).width() >= 768) {
				sandwich.parents(".header__item").addClass("header__item-sandwich");
			}
			sandwich.addClass("is--active");
			nav.addClass("is--active");
			hideScroll();
		}
	})

	/**submenu**/
	subMenu.on("click", function (e) {
		e.preventDefault();
		$(this).toggleClass("is--active");
	})

	menuItem.hover(
		function () {
			menuItem.removeClass("is--hover")
			$(this).addClass("is--hover");
		}
	);

	/**slick-carousel**/
	carousel.each(function () {
		var slider = $(this);
		if (slider.is('.color-palette-list')) {
			slider.slick({
				slidesToShow: 5,
				slidesToScroll: 1,
				prevArrow: $('#arrowPrev'),
				nextArrow: $('#arrowNext'),
				dots: false,
				swipe: false,
				speed: 400,
				infinite: false,
				responsive: [{
					breakpoint: 768,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
						swipe: true
					}
				}]
			})
		} else if (slider.is('.reviews-wrap')) {
			slider.slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: true,
				prevArrow: '<div class="slick-nav slick-nav__prev"></div>',
				nextArrow: '<div class="slick-nav slick-nav__next"></div>',
				dots: false,
				variableWidth: true,
				centerMode: true,
				swipe: true,
				speed: 400,
				infinite: true,
				responsive: [{
					breakpoint: 768,
					settings: {
						arrows: false
					}
				}]
			})
		} else if (slider.is(".overview-wrap")) {
			slider.slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				dots: true,
				variableWidth: false,
				swipe: true,
				speed: 400,
				infinite: false,
				adaptiveHeight: false
			})

			// slider.on('swipe', function (event, slick, direction) {
			// 	initHeight();
			// });

			// slider.on('click', '.slick-dots li', function () {
			// 	initHeight();
			// });

			// initHeight();
		} else if (slider.is(".detail-card__product")) {
			slider.slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				dots: true,
				appendDots: $("#dots"),
				swipe: false,
				speed: 500,
				infinite: false,
				asNavFor: '.detail-card__nav',
				focusOnSelect: true,
				infinite: true,
				responsive: [{
					breakpoint: 768,
					settings: {
						swipe: true
					}
				}]
			})
		} else if (slider.is(".detail-card__nav")) {
			slider.slick({
				slidesToShow: 3,
				slidesToScroll: 3,
				swipe: true,
				speed: 500,
				arrows: false,
				asNavFor: '.detail-card__product',
				focusOnSelect: true,
				centerMode: true,
				variableWidth: true,
				infinite: true,
				responsive: [{
					breakpoint: 768,
					settings: {
						swipe: false
					}
				}]
			})
		}
	});

	/**polyfill object-fit**/
	objectFitPolyfill($('.object-fit'));

	/**close popup on click window**/
	$(document).on("mouseup touchend", function (e) {
		var popup = popups.find('.is--show');
		var popup2 = popups.find('.is--show--secondary');
		
		if (nav.is(':visible') && nav.hasClass('is--active') && $(window).width() >= 768 ) {
			if (!sandwich.is(e.target) && sandwich.has(e.target).length === 0 && !nav.is(e.target) && nav.has(e.target).length === 0) {
				subMenu.removeClass("is--active");
				sandwich.click();
			}
		}

		if (popup.is(':visible') && popups.hasClass('is--active')) {
			if (popup.is(e.target) && popup.has(e.target).length === 0) {
				popup.find(".close").click();
			}
		}

		if (popup2.is(':visible') && popups.hasClass('is--active')) {
			if (popup2.is(e.target) && popup2.has(e.target).length === 0) {
				popup2.find(".close").click();
			}
		}

		if (profileBlank.is(':visible') && profileBlank.hasClass('is--active')) {
			if (!profileBlank.is(e.target) && profileBlank.has(e.target).length === 0 && !$(".user-icon").is(e.target) && $(".user-icon").has(e.target).length === 0) {
				profile.click();
			}
		}
	});
});