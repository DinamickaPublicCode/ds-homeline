$(window).on("scroll", function(){
    var header = $(".header"),
		tagBody = $("body");

    if ($(document).scrollTop() == 0  && tagBody.css("position") != "fixed"){
		header.removeClass("is--scrolling");
	} else {
		header.addClass("is--scrolling");
	}

	fixedColorPallete();
})