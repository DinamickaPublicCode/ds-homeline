$(window).on('resize orientationchange', function() {
    var carousel = $(".slick-carousel"),
        categoryList = $(".category__list");
    
    if ( $(window).width() > 1199 ) {
        categoryList.removeAttr("style");
    }

    /**slick resize**/
    carousel.slick("resize");
   
});