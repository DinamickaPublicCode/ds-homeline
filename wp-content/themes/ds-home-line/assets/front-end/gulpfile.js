let gulp = require('gulp');
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    csso = require('gulp-csso'),

gulp.task('libs-js', function() {
  return gulp.src('app/js/libs/*.js')
    .pipe(concat('libs.min.js'))
    .pipe(uglify({
      toplevel: true
    }))
    .pipe(gulp.dest('app/js/'));
});

gulp.task('libs-css', function() {
  return gulp.src('app/css/libs/*.css')
    .pipe(concat('libs.min.css'))
    .pipe(csso())
    .pipe(gulp.dest('app/css/'));
});

gulp.task('common-js', function() {
  return gulp.src([
    'app/js/events/*.js',
    ])
    .pipe(concat('common.js'))
    .pipe(gulp.dest('app/js/'));
});

gulp.task('sass', function () {
  return gulp.src('app/sass/**/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('browser-sync', function() {
    browserSync({
        // server: {
        //     baseDir: 'app'
        // },
        proxy: "ds-home-line",
        notify: false
    });
});

gulp.task('autoPrefixer', () =>
    gulp.src('app/css/**/*.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('app/css'))
);

gulp.task('watch', ['browser-sync', 'sass', 'libs-js', 'libs-css', 'autoPrefixer'], function () {
  gulp.watch('app/sass/**/*.scss', ['sass']);
  gulp.watch('../*.php', browserSync.reload);
  gulp.watch(['app/js/events/**/*.js'
  ], ['common-js']).on('change', browserSync.reload);
  gulp.watch([
    'app/css/**/*.css'
  ], ['autoPrefixer']).on('change', browserSync.reload);
});