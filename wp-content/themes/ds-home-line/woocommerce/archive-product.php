<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package DsHomeLine
 * @version 1.0.0
 */
defined('ABSPATH') || exit();

get_header();

if ( is_shop() ) {
    wc_get_template_part('archive-product/shop');
} else if ( is_product_category() ) {
    wc_get_template_part('archive-product/category');
}

get_template_part('template-parts/common/section', 'overview');

get_template_part('template-parts/common/section', 'contact');


get_footer();