<?php
/**
 * The Template for displaying all single products
 * 
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package DsHomeLine
 * @version 1.6.4
 */
defined( 'ABSPATH' ) || exit;

get_header(); ?>
<section class="layout__sect">

	<?php woocommerce_breadcrumb(); ?>
	
    <div class="container">
    	<div class="detail-product">
        	<?php
    		/**
    		 * woocommerce_before_main_content hook.
    		 * 
    		 */
    		do_action( 'woocommerce_before_main_content' );
        	?>
        
    		<?php while ( have_posts() ) : the_post(); ?>
    
    			<?php wc_get_template_part( 'content', 'single-product' ); ?>
    
    		<?php endwhile; // end of the loop. ?>
        
        	<?php
    		/**
    		 * woocommerce_after_main_content hook.
    		 */
    		do_action( 'woocommerce_after_main_content' );
        	?>
        </div>
    </div>
</section>

<?php get_template_part('template-parts/common/section', 'find-complect'); ?>

<?php get_template_part('template-parts/common/section', 'overview'); ?>

<?php get_template_part('template-parts/common/section', 'contact'); ?>

<?php get_footer();
/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */