<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$user = wp_get_current_user();
?>

<div class="cabinet__data-img">
    <img src="<?php echo get_template_directory_uri();?>/assets/front-end/app/img/no-image.png" class="object-fit is--cover" data-object-fit="cover" alt="Account Image">
</div>

<div class="cabinet__data-name">
    <h3><?php echo $user->first_name; ?>&nbsp;<?php echo $user->last_name; ?></h3>
</div>

<ul class="cabinet__data-list">
    <li class="cabinet__data-item">
        <p class="cabinet__data-label">E-mail:</p>
        <p class="cabinet__data-text"><a href="mailto:<?php echo $user->user_email; ?>"><?php echo $user->user_email; ?></a></p>
    </li>

<!--             <li class="cabinet__data-item"> -->
<!--                 <p class="cabinet__data-label">Дата рождения:</p> -->
<!--                 <p class="cabinet__data-text">25.02.1997</p> -->
<!--             </li> -->

    <li class="cabinet__data-item">
        <p class="cabinet__data-label">Номер телефона:</p>
        <p class="cabinet__data-text"><a href="tel:<?php echo get_user_meta( $user->ID, 'billing_phone', true ); ?>"><?php echo get_user_meta( $user->ID, 'billing_phone', true ); ?></a></p>
    </li>
</ul>

<div class="cabinet__data-btn">
    <a href="<?php echo esc_url( wc_get_account_endpoint_url( 'edit-account' ) ); ?>" class="btn">Редактировать</a>
</div>

<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
