<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * My Account navigation.
 *
 * @since 2.6.0
 */
//do_action( 'woocommerce_account_navigation' ); ?>

<section class="layout__sect">
    <div class="container">
    
    	<?php woocommerce_breadcrumb(); ?>
    	
    	<div class="layout__info">
            <div class="layout__title">
                <h2>Личный кабинет</h2>
            </div>
        </div>
                    
    	<div class="cabinet">

    		<?php do_action( 'woocommerce_account_navigation' );?>
    		
    		<?php if (!is_wc_endpoint_url( 'orders' )) { ?>
			<div class="cabinet__data">
			<?php } ?>
        	<?php
        		/**
        		 * My Account content.
        		 *
        		 * @since 2.6.0
        		 */
        		do_action( 'woocommerce_account_content' );
        	?>
        	<?php if (!is_wc_endpoint_url( 'orders' )) { ?>
        	</div>
        	<?php } ?>
    	</div>
    	
	</div>
</section>
