<?php
/**
 * Orders
 *
 * Shows orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/orders.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

?>
<?php if ( $has_orders ) : ?>
<?php 
    foreach ( $customer_orders->orders as $customer_order ) {
        $order      = wc_get_order( $customer_order ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
    ?>
<div class="cabinet__cart cart-tbl cart-tbl--layout">
  <div class="cabinet__cart-head">
    <ul class="cabinet__cart-items">
      <?php foreach ( wc_get_account_orders_columns() as $column_id => $column_name ) { ?>
      <li class="cabinet__cart-item">
        <?php if ( 'order-number' === $column_id ) { ?>
        <p>Номер заказа: <span><?php echo $order->get_order_number(); ?></span></p>
        <?php } else if ( 'order-date' === $column_id ) { ?>
        <p>Дата: <span><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?></span></p>
        <?php } else if  ( 'order-status' === $column_id ) { ?>
        <p>Статус: <span><?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?></span></p>
        <?php } else if  ( 'order-total' === $column_id ) { ?>
        <p>Сумма заказа:
          <span><strong><?php echo  $order->get_total(); ?>&nbsp;<?php echo get_woocommerce_currency_symbol(); ?></strong></span>
        </p>
        <?php } ?>
      </li>
      <?php } ?>
    </ul>
  </div>
  <?php foreach ($order->get_items() as $item) { ?>
  <?php $_product = $item->get_product(); ?>
  <div class="cart-tbl__row cabinet__cart-row">
    <div class="cart-tbl__info is--mob">
      <div class="cart-tbl__text">
        <p class="cart-tbl__name"><strong><?php echo $item->get_name(); ?></strong>
        </p>
      </div>
      <div class="cart-tbl__descr">
        <?php 
                    $variation_attributes = $_product->get_variation_attributes();
                    foreach($variation_attributes as $attribute_taxonomy => $term_slug){
                        $taxonomy = str_replace('attribute_', '', $attribute_taxonomy );
                        $attribute_value = get_term_by( 'slug', $term_slug, $taxonomy )->name;
                        echo sprintf('<p>%s</p>', $attribute_value);
                    }
                    ?>
      </div>
    </div>
    <div class="cart-tbl__img">
      <img src="<?php echo wp_get_attachment_url( $_product->get_image_id() ); ?>" alt="card" data-object-fit="contain"
        class="object-fit is--contain">
    </div>
    <div class="cart-tbl__info">
      <div class="cart-tbl__text">
        <p class="cart-tbl__name"><strong><?php echo $item->get_name(); ?></strong></p>
      </div>
      <div class="cart-tbl__descr">
        <?php 
                    $variation_attributes = $_product->get_variation_attributes();
                    foreach($variation_attributes as $attribute_taxonomy => $term_slug){
                        $taxonomy = str_replace('attribute_', '', $attribute_taxonomy );
                        $attribute_value = get_term_by( 'slug', $term_slug, $taxonomy )->name;
                        echo sprintf('<p>%s</p>', $attribute_value);
                    }
                    ?>
      </div>

      <div class="cart-tbl__items">
        <div class="cart-tbl__item">
          <div class="cart-tbl__label">
            <p>Кол-во:</p>
          </div>

          <div class="counter counter--secondary">
            <p class="counter__text"><?php echo $item->get_quantity(); ?> шт.</p>
          </div>
        </div>
        <div class="cart-tbl__item">
          <div class="cart-tbl__label">
            <p>Цена:</p>
          </div>
          <h3><span><?php echo ($_product->get_price()); ?></span> грн</h3>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<?php } ?>

<?php else : ?>
<div
  class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info">
  <a class="woocommerce-Button button"
    href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
    <?php esc_html_e( 'Go to the shop', 'woocommerce' ); ?>
  </a>
  <?php esc_html_e( 'No order has been made yet.', 'woocommerce' ); ?>
</div>
<?php endif; ?>