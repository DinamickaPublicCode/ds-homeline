<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<section class="layout__sect">

  <div class="container">

    <?php woocommerce_breadcrumb(); ?>


    <div class="layout__info" data-tabs>

      <div class="layout__info-title">
        <h3><?php _e('Вход в аккаунт', 'dhl'); ?></h3>
      </div>
      <?php if ( 'yes' === get_option( 'woocommerce_enable_myaccount_registration' ) ) : ?>
      <ul class="layout__info-items">
        <li class="layout__info-item">
          <a href="javascript:void(0);" class="layout__info-link is--active"
            data-tab="login"><?php _e('Войти', 'dhl'); ?></a>
        </li>
        <li class="layout__info-item">
          <a href="#register" class="layout__info-link"
            data-tab="register"><?php _e('Зарегестрироваться', 'dhl'); ?></a>
        </li>
      </ul>


      <?php endif; 
      do_action( 'woocommerce_before_customer_login_form' ); ?>
			
			<div class="auth-social auth-social--secondary">
          <div class="auth-social__links">
            <a href="#" class="auth-social__link is--fb">Регистрация с Facebook</a>

            <a href="#" class="auth-social__link is--google">Регистрация с Google</a>
          </div>

          <div class="auth-social__divider">
            <p>или</p>
          </div>
        </div>

      <form class="woocommerce-form woocommerce-form-login login form form--secondary form--third is--active"
        method="post" data-tab-content="login">
        
        <?php do_action( 'woocommerce_login_form_start' ); ?>
        <div class="form__items">
          <div class="form__item">
            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username"
              id="username" autocomplete="username" placeholder="E-mail"
              value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
          </div>
        </div>
        <div class="form__items">
          <div class="form__item">
            <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password"
              id="password" placeholder="******" autocomplete="current-password" />
          </div>
        </div>

        <?php do_action( 'woocommerce_login_form' ); ?>

        <div class="form__item-link form__item">
          <a class="btn-link"
            href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'woocommerce' ); ?></a>
        </div>

        <div class="form__item">
          <!--     				<label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme"> -->
          <!--     					<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> -->
          <?php /*<span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span> */ ?>
          <!--     				</label> -->
          <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
          <button type="submit" class="woocommerce-button button woocommerce-form-login__submit btn" name="login"
            value="<?php esc_attr_e( 'Log in', 'woocommerce' ); ?>"><?php esc_html_e( 'Log in', 'woocommerce' ); ?></button>
        </div>
        <?php do_action( 'woocommerce_login_form_end' ); ?>
      </form>

      <?php if ( 'yes' === get_option( 'woocommerce_enable_myaccount_registration' ) ) : ?>

      <form method="post" class="woocommerce-form woocommerce-form-register register form form--secondary form--third"
        data-tab-content="register" <?php do_action( 'woocommerce_register_form_tag' ); ?>>

        <?php do_action( 'woocommerce_register_form_start' ); ?>

        <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

        <div class="form__items">
          <div class="form__item">
            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" placeholder="Логин"
              name="email" id="reg_username" autocomplete="username"
              value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
          </div>
        </div>

        <?php endif; ?>

        <div class="form__items">
          <div class="form__item">
            <input type="text" name="first_name" placeholder="Имя">
          </div>
        </div>

        <div class="form__items">
          <div class="form__item">
            <input type="text" name="last_name" placeholder="Фамилия">
          </div>
        </div>

        <div class="form__items">
          <div class="form__item">
            <input type="tel" id="phone" placeholder="+380-__-___-__-__" name="billing_phone" data-phone>
          </div>
        </div>

        <div class="form__items">
          <div class="form__item">
            <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email"
              placeholder="E-mail" autocomplete="email"
              value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
          </div>
        </div>

        <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
        <div class="form__items">
          <div class="form__item">
            <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password"
              id="reg_password" placeholder="Пароль" autocomplete="new-password" />
          </div>
        </div>
        <?php else : ?>
        <div class="form__items">
          <div class="form__item">
            <p><?php esc_html_e( 'A password will be sent to your email address.', 'woocommerce' ); ?></p>
          </div>
        </div>
        <?php endif; ?>

        <?php do_action( 'woocommerce_register_form' ); ?>
        <div class="form__item">
          <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
          <button type="submit" class="woocommerce-Button button btn" name="register"
            value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Register', 'woocommerce' ); ?></button>
        </div>
        <?php do_action( 'woocommerce_register_form_end' ); ?>

      </form>

      <?php endif; ?>

    </div>

  </div>

</section>


<?php do_action( 'woocommerce_after_customer_login_form' ); ?>