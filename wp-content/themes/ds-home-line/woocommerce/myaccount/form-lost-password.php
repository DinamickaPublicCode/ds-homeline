<?php
/**
 * Lost password form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-lost-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.2
 */

defined( 'ABSPATH' ) || exit;


?>
<section class="layout__sect">

	<div class="container">
	
		<?php woocommerce_breadcrumb(); ?>
		
		<div class="layout__info">
						<?php do_action( 'woocommerce_before_lost_password_form' ); ?>
            <form method="post" class="woocommerce-ResetPassword lost_reset_password form">
            
            	<div class="form__items">
            		<div class="form__item">
            			<p><?php echo apply_filters( 'woocommerce_lost_password_message', esc_html__( 'Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.', 'woocommerce' ) ); ?></p><?php // @codingStandardsIgnoreLine ?>
            		</div>
            	</div>
            	
            	<div class="form__items">
            		<div class="form__item">
                		<label for="user_login" style="display: block; margin-bottom: 10px;"><?php esc_html_e( 'Username or email', 'woocommerce' ); ?></label>
                		<input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="user_login" id="user_login" autocomplete="username" />
            		</div>
            	</div>
            	
            
            	<?php do_action( 'woocommerce_lostpassword_form' ); ?>
            
            	<div class="form__items">
            		<div class="form__item">
                		<input type="hidden" name="wc_reset_password" value="true" />
                		<button type="submit" class="woocommerce-Button button btn" value="<?php esc_attr_e( 'Reset password', 'woocommerce' ); ?>"><?php esc_html_e( 'Reset password', 'woocommerce' ); ?></button>
            		</div>
            	</div>
            
            	<?php wp_nonce_field( 'lost_password', 'woocommerce-lost-password-nonce' ); ?>
            
            </form>
        </div>
	</div>
</section>
<?php
do_action( 'woocommerce_after_lost_password_form' );
