<?php
/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/mini-cart.php.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package DsHomeLine
 * @version 3.7.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$count = WC()->cart->get_cart_contents_count();

if ((2 <= $count) && ($count <= 4)) {
    $multiple_count = 'товара';
} else {
    $multiple_count = 'товаров';
}

$item_count_text = sprintf(
    _n( '<span>%d</span> товар', '<span>%d</span> ' . $multiple_count, $count, 'dhl' ),
    $count
);

do_action( 'woocommerce_before_mini_cart' ); ?>


<div class="cart-tbl mini-cart-data">
	
    <div class="cart-tbl__head">
        <div class="cart-tbl__items">
            <div class="cart-tbl__item">
                <div class="cart-tbl__title">
                	<h3><?php _e('Корзина', 'dhl')?></h3>
                </div>
            </div>
            <div class="cart-tbl__item">
                <div class="cart-tbl__value">
                	<p><?php echo $item_count_text; ?></p>
                </div>
            </div>
        </div>
        <div class="close" data-name-popup="cart-tbl">
        	<span class="close__part"></span>
        	<span class="close__part"></span>
        </div>
    </div>
	
	<div class="cart-tbl__body">
    <?php if ( ! WC()->cart->is_empty() ) { ?>
    
    		<?php
    			do_action( 'woocommerce_before_mini_cart_contents' );
    
                $first_product = null;
                $cartProducts = array_reverse(WC()->cart->get_cart());
    			foreach ( $cartProducts as $cart_item_key => $cart_item ) {
    			    
    				$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
    				
    				if (is_null($first_product)) {
    				    $first_product = $_product;
    				}
    				
    				$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
    
    				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
    					?>
    					<div class="cart-tbl__row" <?php echo esc_attr( apply_filters( 'woocommerce_mini_cart_item_class', 'mini_cart_item', $cart_item, $cart_item_key ) ); ?>>
                            <div class="cart-tbl__img">
                            	<img src="<?php echo wp_get_attachment_url( $_product->get_image_id() ); ?>" />
                            </div>
                            <div class="cart-tbl__info">
                                <div class="cart-tbl__text">
                                	<p class="cart-tbl__name"><strong><?php echo $cart_item['data']->get_title(); ?></strong></p>
    								<?php
            						echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
            							'<a class="remove-icon-wrap remove remove_from_cart_button" href="%s" aria-label="%s" data-product_id="%s" data-cart_item_key="%s" data-product_sku="%s" data-id="remove">
                                            <svg class="remove__icon">
                                              <use class="remove__icon-part" xlink:href="%s/assets/front-end/app/img/icons/sprite.svg#remove"></use>
                                            </svg>
                                        </a>',
            							esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
            							__( 'Remove this item', 'woocommerce' ),
            							esc_attr( $product_id ),
            							esc_attr( $cart_item_key ),
            							esc_attr( $_product->get_sku() ),
            						    get_template_directory_uri()
            						), $cart_item_key );
            						?>
                                </div>
                                <div class="cart-tbl__descr">
                                    <?php 
                                    $variation_attributes = $_product->get_variation_attributes();
                                    foreach($variation_attributes as $attribute_taxonomy => $term_slug){
                                        $taxonomy = str_replace('attribute_', '', $attribute_taxonomy );
                                        $attribute_value = get_term_by( 'slug', $term_slug, $taxonomy )->name;
                                        echo sprintf('<p>%s</p>', $attribute_value);
                                    }
                                    ?>	
                                </div>
                                <div class="cart-tbl__bottom">
                                	<div class="counter">
                                        <span class="counter__nav" data-counter-prev>
                                            <svg class="counter-icon">
                                            	<use class="counter-icon__part" xlink:href="<?php echo get_template_directory_uri();?>/assets/front-end/app/img/icons/sprite.svg#minus"></use>
                                            </svg>
                                        </span>
                                    
                                    	<input min="1" max="999" step="1" value="<?php echo $cart_item['quantity']; ?>" type="number" pattern="\d [0-9]"
                                    	data-product_id="<?php echo $cart_item['product_id']; ?>"
                                        data-variation_id="<?php echo $cart_item['variation_id']; ?>"
                                        data-cart_item_key="<?php echo $cart_item_key;?>">
                                    
                                        <span class="counter__nav" data-counter-next>
                                            <svg class="counter-icon">
                                            	<use class="counter-icon__part" xlink:href="<?php echo get_template_directory_uri();?>/assets/front-end/app/img/icons/sprite.svg#plus"></use>
                                            </svg>
                                        </span>
                                	</div>
                                	<p><span><?php echo ($_product->get_price() * $cart_item['quantity']); ?></span> грн</p>
                                </div>
                            </div>
    					</div>
    					<?php
    				}
    			}
    
    			do_action( 'woocommerce_mini_cart_contents' );
    		?>
    
    	<?php do_action( 'woocommerce_widget_shopping_cart_before_buttons' ); ?>
    
    <?php } else { ?>
    	
        <div class="cart-tbl__msg">
            <div class="cart-tbl__icon">
                <svg class="shopping-icon">
                	<use class="shopping-icon__part" xlink:href="<?php echo get_template_directory_uri();?>/assets/front-end/app/img/icons/sprite.svg#shopping-cart"></use>
                </svg>
            </div>
        
        	<h3>Ваша корзина пустая :(</h3>
        </div>
    
    <?php }; ?>
    
    </div>
    
    
    <div class="cart-tbl__footer">
    	<?php if ( ! WC()->cart->is_empty() ) { ?>
        <div class="cart-tbl__text">
        	<p class="cart-tbl__stext">Cумма заказа:</p>
        	<p><strong><?php echo WC()->cart->get_cart_subtotal(); ?></strong></p>
        </div>
        <div class="cart-tbl__alert">
        	<p>Если у вас есть подарочный промокод, введите его на следующем шаге.</p>
        </div>
        <ul class="cart-tbl__btns" data-list-btn="list-btn">
            <li class="cart-tbl__btn">
            	<a href="<?php echo wc_get_checkout_url(); ?>" class="btn">Оформить заказ</a>
            </li>
            <li class="cart-tbl__btn">
            	<a href="javascript:void(0);" class="btn btn--fourth buy-on-fly">Купить в один клик</a>
            </li>
        </ul>
        <?php } else { ?>
        <div class="cart-tbl__alert">
        	<p>Выберите свой комплект постельного белья в нашем магазине.</p>
        </div>
        <ul class="cart-tbl__btns" data-list-btn="list-btn">
            <li class="cart-tbl__btn">
            	<a href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>" class="btn">Выбрать свой комплект</a>
            </li>
        </ul>
        <?php } ?>
    </div>

	<?php 
	if ( !WC()->cart->is_empty() && isset($first_product) && !is_null($first_product) ) {
	    
    $crossSells = $first_product->get_cross_sell_ids();

    if (!empty($crossSells)) {
    $crossProductID = array_shift($crossSells);
    $crossProduct =  wc_get_product( $crossProductID );
	?>
        <div class="cart-tbl__block">
            <div class="cart-tbl__img">
                <img src="<?php echo wp_get_attachment_url( $crossProduct->get_image_id() ); ?>" alt="card">
            </div>
            <div class="cart-tbl__info">
                <div class="cart-tbl__text">
                    <p class="cart-tbl__name"><strong><?php echo $crossProduct->get_title();?></strong></p>
                </div>
                <div class="cart-tbl__descr">
                    <?php 
                    $variation_attributes = $crossProduct->get_variation_attributes();
                    foreach($variation_attributes as $attribute_taxonomy => $term_slug){
                        $taxonomy = str_replace('attribute_', '', $attribute_taxonomy );
                        $attribute_value = get_term_by( 'slug', $term_slug, $taxonomy )->name;
                        echo sprintf('<p>%s</p>', $attribute_value);
                    }
                    ?>	          
                </div>
                <div class="cart-tbl__bottom">
                    <a id="crosssell-add-to-cart" href="javascript:void(0);" class="btn btn--fifth"
                    data-product_id="<?php echo absint( $crossProduct->get_parent_id() ); ?>"
                    data-quantity="1"
                    data-variation_id="<?php echo absint( $crossProduct->get_id() ); ?>">Добавить в корзину</a>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php } ?>
</div>

<?php do_action( 'woocommerce_after_mini_cart' ); ?>
