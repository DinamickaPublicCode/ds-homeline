<?php
/**
 * Empty cart page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-empty.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */
defined('ABSPATH') || exit();

/*
 * @hooked wc_empty_cart_message - 10
 */
// do_action('woocommerce_cart_is_empty');

if (wc_get_page_id('shop') > 0) :?>
<section class="layout__sect">

	<div class="container">
		
		<?php woocommerce_breadcrumb(); ?>

		<div class="layout__title">
			<h2>Корзина</h2>
		</div>

		<div class="layout__info">
			<div class="cart-tbl cart-tbl--layout">
				<div class="cart-tbl__icon">
					<svg class="shopping-icon">
                    	<use class="shopping-icon__part" xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#shopping-cart"></use>
                	</svg>
				</div>
				<div class="cart-tbl__msg">
					<h3>Ваша корзина пустая :(</h3>
				</div>
			</div>
			<div class="full-content">
				<p>Выберите свой комплект постельного белья <br> в нашем магазине.</p>
			</div>
			<a class="btn" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
    			<?php esc_html_e( 'Продолжить поиск', 'dhl' ); ?>
    		</a>
		</div>
		
	</div>
	
</section>
<?php endif; ?>
