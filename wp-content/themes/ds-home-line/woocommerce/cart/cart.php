<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */
defined('ABSPATH') || exit();

do_action('woocommerce_before_cart');
?>
<section class="layout__sect full-cart">
    <?php woocommerce_breadcrumb(); ?>

    <div class="container">

        <div class="layout__title">
            <h2>Корзина</h2>
        </div>

        <div class="layout__column">
            <div class="cart-tbl cart-tbl--layout">
                <?php
                foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                    $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                    $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);
                
                    if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                        $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
                        ?>
                <div class="cart-tbl__row">
                    <div class="cart-tbl__info is--mob">
                        <div class="cart-tbl__text">
                            <p class="cart-tbl__name"><strong><?php echo $cart_item['data']->get_title(); ?></strong>
                            </p>
                            <?php
                            		echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                            		    '<a class="remove-icon-wrap remove remove_from_cart_button" href="%s" aria-label="%s" data-product_id="%s" data-cart_item_key="%s" data-product_sku="%s" data-id="remove">
                                                                        <svg class="remove__icon">
                                                                          <use class="remove__icon-part" xlink:href="%s/assets/front-end/app/img/icons/sprite.svg#remove"></use>
                                                                        </svg>
                                                                    </a>',
                            		    esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
                            		    __( 'Remove this item', 'woocommerce' ),
                            		    esc_attr( $product_id ),
                            		    esc_attr( $cart_item_key ),
                            		    esc_attr( $_product->get_sku() ),
                            		    get_template_directory_uri()
                            		    ), $cart_item_key );
                                    ?>
                        </div>
                        <div class="cart-tbl__descr">
                            <?php 
                                    $variation_attributes = $_product->get_variation_attributes();
                                    foreach($variation_attributes as $attribute_taxonomy => $term_slug){
                                        $taxonomy = str_replace('attribute_', '', $attribute_taxonomy );
                                        $attribute_value = get_term_by( 'slug', $term_slug, $taxonomy )->name;
                                        echo sprintf('<p>%s</p>', $attribute_value);
                                    }
                                    ?>
                        </div>
                    </div>
                    <div class="cart-tbl__img">
                        <img src="<?php echo wp_get_attachment_url( $_product->get_image_id() ); ?> " alt="card"
                            data-object-fit="contain" class="object-fit is--contain">
                    </div>
                    <div class="cart-tbl__info">
                        <div class="cart-tbl__text">
                            <p class="cart-tbl__name"><strong><?php echo $cart_item['data']->get_title(); ?></strong>
                            </p>
                            <?php
                            		echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                            		    '<a class="remove-icon-wrap remove remove_from_cart_button" href="%s" aria-label="%s" data-product_id="%s" data-cart_item_key="%s" data-product_sku="%s" data-id="remove">
                                                                        <svg class="remove__icon">
                                                                          <use class="remove__icon-part" xlink:href="%s/assets/front-end/app/img/icons/sprite.svg#remove"></use>
                                                                        </svg>
                                                                    </a>',
                            		    esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
                            		    __( 'Remove this item', 'woocommerce' ),
                            		    esc_attr( $product_id ),
                            		    esc_attr( $cart_item_key ),
                            		    esc_attr( $_product->get_sku() ),
                            		    get_template_directory_uri()
                            		    ), $cart_item_key );
                                    ?>
                        </div>
                        <div class="cart-tbl__descr">
                            <?php 
                                    $variation_attributes = $_product->get_variation_attributes();
                                    foreach($variation_attributes as $attribute_taxonomy => $term_slug){
                                        $taxonomy = str_replace('attribute_', '', $attribute_taxonomy );
                                        $attribute_value = get_term_by( 'slug', $term_slug, $taxonomy )->name;
                                        echo sprintf('<p>%s</p>', $attribute_value);
                                    }
                                    ?>
                        </div>
                        <div class="cart-tbl__items">
                            <div class="cart-tbl__item">
                                <div class="cart-tbl__label">
                                    <p>Кол-во:</p>
                                </div>
                                <div class="counter counter--secondary">
                                    <span class="counter__nav" data-counter-prev>
                                        <svg class="counter-icon">
                                            <use class="counter-icon__part"
                                                xlink:href="<?php echo get_template_directory_uri();?>/assets/front-end/app/img/icons/sprite.svg#minus">
                                            </use>
                                        </svg>
                                    </span>

                                    <input min="1" max="999" step="1" value="<?php echo $cart_item['quantity']; ?>"
                                        type="number" pattern="\d [0-9]"
                                        data-product_id="<?php echo $cart_item['product_id']; ?>"
                                        data-variation_id="<?php echo $cart_item['variation_id']; ?>"
                                        data-cart_item_key="<?php echo $cart_item_key;?>">

                                    <span class="counter__nav" data-counter-next>
                                        <svg class="counter-icon">
                                            <use class="counter-icon__part"
                                                xlink:href="<?php echo get_template_directory_uri();?>/assets/front-end/app/img/icons/sprite.svg#plus">
                                            </use>
                                        </svg>
                                    </span>
                                </div>
                            </div>
                            <div class="cart-tbl__item card-tbl__remove">
                                <?php
                            		echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                            		    '<a class="remove-icon-wrap remove remove_from_cart_button" href="%s" aria-label="%s" data-product_id="%s" data-cart_item_key="%s" data-product_sku="%s" data-id="remove">
                                            <svg class="remove__icon">
                                                <use class="remove__icon-part" xlink:href="%s/assets/front-end/app/img/icons/sprite.svg#remove"></use>
                                            </svg>
                                        </a>',
                            		    esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
                            		    __( 'Remove this item', 'woocommerce' ),
                            		    esc_attr( $product_id ),
                            		    esc_attr( $cart_item_key ),
                            		    esc_attr( $_product->get_sku() ),
                            		    get_template_directory_uri()
                            		    ), $cart_item_key );
                                    ?>
                            </div>
                            <div class="cart-tbl__item">
                                <div class="cart-tbl__label">
                                    <p>Цена:</p>
                                </div>
                                <h3><span><?php echo ($_product->get_price() * $cart_item['quantity']); ?></span> грн
                                </h3>
                            </div>
                        </div>

                    </div>
                </div>
                <?php
                    }
                }
                ?>
            </div>
            <aside class="aside" role="complementary">
                <div class="aside__row">
                    <div class="aside__title">
                        <h4>Информация о заказе</h4>
                    </div>
                    <div class="aside__item">
                        <div class="cart-tbl cart-tbl--layout">
                            <div class="cart-tbl__text">
                                <p class="cart-tbl__stext">Cумма заказа:</p>
                                <p><strong><?php echo WC()->cart->get_cart_subtotal(); ?></strong></p>
                            </div>
                            <div class="cart-tbl__stext">
                                <p><strong>Если у вас есть подарочный промокод,введите его на следующем шаге.</strong>
                                </p>
                            </div>
                        </div>
                        <div class="aside__item-btn">
                            <ul class="cart-tbl__btns" data-list-btn="list-btn">
                                <li class="cart-tbl__btn">
                                    <a href="<?php echo wc_get_checkout_url(); ?>" class="btn">Оформить заказ</a>
                                </li>
                                <li class="cart-tbl__btn">
                                    <a href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>" class="btn btn--fourth">Продолжить покупки</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </aside>
        </div>
    </div>
</section>