<?php 
/**
 * The Template for displaying category archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product/shop.php
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package DsHomeLine
 * @version 1.0.0
 */
defined('ABSPATH') || exit();

$args = array(
    'taxonomy'  => 'product_cat',
    'hide_empty' => true,
);
$terms = get_terms($args);

?>
<section class="layout__sect">
	
	<?php woocommerce_breadcrumb(); ?>
	
	<div class="container">
	
		<div class="layout__title">
			<h2><?php _e('Все товары', 'dhl'); ?></h2>
		</div>
		
		<?php if (!empty($terms)) { ?>
		<div class="category-blanks">
		
			<?php 
			foreach ($terms as $term) {
			    $thumbnail_id = get_term_meta( $term->term_id, 'thumbnail_id', true );
			    $image = wp_get_attachment_url( $thumbnail_id );
			    
			    $useCustomLink = get_term_meta( $term->term_id, 'dhl_category_custom_link_status', true );
			    if ($useCustomLink) {
			        $link = get_term_meta( $term->term_id, 'dhl_category_custom_link', true );
			    } else {
			        $link = get_category_link( $term->term_id );
			    }
			?>
			<a href="<?php echo $link; ?>" class="category__blank">
				<div class="category__blank-img">
					<img class="object-fit is--contain" data-object-fit="contain" src="<?php echo $image; ?>" alt="<?php echo $term->name; ?>">
				</div>

				<div class="cacategory__blank-title">
					<h3><?php echo $term->name; ?></h3>
				</div>
			</a> 
			<?php } ?>
			
			<div class="category__blank in--developing">
				<div class="category__blank-img">
					<img class="object-fit is--contain" data-object-fit="contain"
						src="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/Bright-Background.png" alt="В разработке">

					<h3 class="category__blank-msg"><?php _e('В разработке', 'dhl'); ?></h3>
				</div>

				<div class="cacategory__blank-title">
					<h3><?php _e('Собери свой комплект', 'dhl'); ?></h3>
				</div>
			</div>
			
		</div>
		<?php } ?>
		
	</div>
	
</section>