<?php 
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product/category.php
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package DsHomeLine
 * @version 1.0.0
 */
defined('ABSPATH') || exit();

?>

<?php get_template_part('template-parts/homepage/section', 'products'); ?>

<section class="layout__sect layout__sect--bg" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/7320.png');">
	<div class="container">
		<div class="layout__title">
			<h2>Выбери свой идеальный комплект</h2>
		</div>
		<div class="layout__info">
			<div class="full-content">
				<p>
					Смелее! Сколько можно пользоваться постельным
					с «бабушкиными» принтами и грубой тканью.
					Одна ночь в нашей постели изменит полностью
					Ваше отношение к тканям.
				</p>
		</div>
	</div>
</section>