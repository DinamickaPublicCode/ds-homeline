<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package DsHomeLine
 * @version 3.5.0
 * 
 * @var object $checkout
 */
if (! defined('ABSPATH')) {
    exit();
}

do_action('woocommerce_before_checkout_form', $checkout);
?>
<section class="layout__sect">

	<div class="container">
	
		<?php woocommerce_breadcrumb(); ?>
		
		<div class="layout__column">

			<?php 
			if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
			    wc_get_template_part('checkout/form', 'login');
			} else { 
			    wc_get_template('checkout/form-delivery.php', array( 'checkout' => $checkout ));
			}
			
			wc_get_template_part('checkout/aside');
			?>
			
		</div>
		
	</div>
	
</section>
<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>

