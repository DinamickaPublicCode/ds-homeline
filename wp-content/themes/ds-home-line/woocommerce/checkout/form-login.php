<?php
/**
 * Checkout login form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-login.php.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package DsHomeLine
 * @version 3.4.0
 */
defined('ABSPATH') || exit();

if (is_user_logged_in() || 'no' === get_option('woocommerce_enable_checkout_login_reminder')) {
    return;
}
?>
<form id="checout-registration" class="form-steps">

  <fieldset class="form__step is--active--step" data-tabs>

    <div class="form__step-title">
      <h3>Информация о покупателе</h3>
    </div>

    <ul class="form__step-items">
      <li class="form__step-item">
        <a href="#" class="form__step-link is--active" data-tab="1">Я новый покупатель</a>
      </li>
      <li class="form__step-item">
        <a href="#" class="form__step-link" data-tab="2">Войти</a>
      </li>
    </ul>

    <!-- REGISTRATION FORM -->
    <div class="form__step-contains is--active" data-tab-content="1">

      <div class="auth-social">
        <div class="auth-social__text">
          <p>Вы можете зарегистрироваться в нашем магазине с помощью учетных записей социальных сетей</p>
        </div>

        <div class="auth-social__links">
          <a href="#" class="auth-social__link is--fb">Регистрация с Facebook</a>

          <a href="#" class="auth-social__link is--google">Регистрация с Google</a>
        </div>

        <div class="auth-social__divider">
          <p>или</p>
        </div>
      </div>

      <div class="form form--secondary">

        <div class="form__items">
          <div class="form__item form__item-label">
            <label for="name">Ваше имя:</label>
          </div>
          <div class="form__item">
            <input type="text" placeholder="Имя" id="name" name="first_name">
          </div>
        </div>

        <div class="form__items">
          <div class="form__item form__item-label">
            <label for="surname">Ваша Фамилия:</label>
          </div>
          <div class="form__item">
            <input type="text" placeholder="Фамилия" id="surname" name="last_name">
          </div>
        </div>

        <div class="form__items">
          <div class="form__item form__item-label">
            <label for="email">E-mail:</label>
          </div>
          <div class="form__item">
            <input type="email" placeholder="E-mail" id="email" name="email">
          </div>
        </div>

        <div class="form__items">
          <div class="form__item form__item-label"></div>
          <div class="form__item checkbox">
            <input type="checkbox" id="check" name="subscribe" checked>
            <label for="check" class="checkbox__part">Держите меня в курсе новостей и эксклюзивных предложений</label>
          </div>
        </div>

        <div class="form__items">
          <div class="form__item form__item-label">
            <label for="phone">Номер телефона:</label>
          </div>
          <div class="form__item">
            <input type="tel" id="phone" placeholder="+380-__-___-__-__" name="billing_phone" data-phone>
          </div>
        </div>

      </div>

    </div>

    <!-- LOGIN FORM -->
    <div class="form__step-contains" data-tab-content="2">

      <div class="auth-social">
        <div class="auth-social__text">
          <p>Вы можете зарегистрироваться в нашем магазине с помощью учетных записей социальных сетей</p>
        </div>

        <div class="auth-social__links">
          <a href="#" class="auth-social__link is--fb">Регистрация с Facebook</a>

          <a href="#" class="auth-social__link is--google">Регистрация с Google</a>
        </div>

        <div class="auth-social__divider">
          <p>или</p>
        </div>
      </div>

      <div class="form form--secondary">

        <div class="form__items">
          <div class="form__item form__item-label">
            <label for="email2">E-mail:</label>
          </div>
          <div class="form__item">
            <input type="text" placeholder="E-mail" id="email2" name="email">
          </div>
        </div>

        <div class="form__items">
          <div class="form__item form__item-label">
            <label for="password2">Пароль:</label>
          </div>

          <div class="form__item">
            <input type="password" placeholder="******" id="password2" name="password">
          </div>
        </div>

        <div class="form__item-link">
          <a href="<?php echo wc_lostpassword_url(); ?>" class="btn-link"><span>Забыли пароль?</span></a>
        </div>

      </div>

    </div>

    <ul class="form__btns">
      <li class="form__btn">
        <input id="checkout-reg-btn" type="submit" class="btn" value="Продолжить" />
      </li>
    </ul>

  </fieldset>
</form>