<?php
/**
 * Shipping Methods Display
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/shipping-method.php.
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package DsHomeLine
 * @version 1.0.0
 * 
 * @var array $available_methods
 * @see dhl_cart_totals_shipping_html();
 * 
 */
defined( 'ABSPATH' ) || exit;

$formatted_destination    = isset( $formatted_destination ) ? $formatted_destination : WC()->countries->get_formatted_address( $package['destination'], ', ' );
$has_calculated_shipping  = ! empty( $has_calculated_shipping );
$show_shipping_calculator = ! empty( $show_shipping_calculator );
$calculator_text          = '';
?>
<?php if ( $available_methods ) { ?>
<div class="form__items">
	<div class="form__item form__item-label">
		<label for="shipping_method">Способ доставки:</label>
	</div>
	<div class="form__item" data-select-wrap>
		<div class="select">
			<select id="shipping_method" name="shipping_rate_method">
				<?php 
				$firstMethod = null;
				foreach ( $available_methods as $method ) { 
				    if (is_null($firstMethod)) {
				        $firstMethod = $method;
				    }
				?>
				<option data-index="<?php echo $index; ?>" data-method_id="<?php echo esc_attr( $method->id ); ?>" value="<?php echo $method->instance_id; ?>"><?php echo $method->label; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="select-clone">
			<div class="select-clone__title" data-select="1">
				<p><?php echo $firstMethod->label; ?></p>
				<div class="select-clone__icon">
					<svg class="arrow-icon">
						<use class="arrow-icon__part is--dark" xlink:href="<?php echo get_template_directory_uri();?>/assets/front-end/app/img/icons/sprite.svg#right-arrow"></use>
					</svg>
				</div>
			</div>
			<ul class="select-clone__options" data-option="1">
				<?php foreach ( $available_methods as $method ) { ?>
				<li class="select-clone__option" value="<?php echo $method->instance_id; ?>"><?php echo $method->label; ?></li>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>
<?php } ?>