<?php
/**
 * Checkout aside block
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/aside.php.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package DsHomeLine
 * @version 1.0.0
 */
defined('ABSPATH') || exit();
?>
<aside class="aside" role="complementary">
  <div class="aside__row">

    <div class="aside__title">
      <h4>Информация о заказе</h4>
    </div>

    <div class="aside-toggle">
      <div class="aside-toggle__text"><span id="aside-text">Спрятать заказ</span>
        <svg class="arrow-icon">
          <use class="arrow-icon__part is--dark"
            xlink:href="<?php echo get_template_directory_uri();?>/assets/front-end/app/img/icons/sprite.svg#right-arrow">
          </use>
        </svg>
      </div>
      <span class="aside-toggle__cost"><?php echo WC()->cart->get_cart_subtotal(true); ?></span>
    </div>

    <div class="aside__item">
      <div class="cart-tbl cart-tbl--secondary">
        <div class="cart-tbl__body">

          <?php wc_get_template_part('checkout/aside/products'); ?>

          <?php wc_get_template_part('checkout/form', 'coupon'); ?>


          <div class="cart-tbl__row">
            <div class="cart-tbl__text">
              <p class="cart-tbl__stext">Cумма заказа:</p>
              <p><strong><?php echo WC()->cart->get_cart_subtotal(); ?></strong></p>
            </div>
          </div>

          <div class="cart-tbl__row">
            <div class="cart-tbl__text">
              <p class="cart-tbl__stext">К оплате:</p>
              <h3><?php echo WC()->cart->get_cart_subtotal(); ?></h3>
            </div>
          </div>

          <a href="<?php echo wc_get_cart_url(); ?>" class="btn btn--fourth">Редактировать</a>

        </div>
      </div>
    </div>

  </div>
</aside>