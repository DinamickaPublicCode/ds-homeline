<?php
/**
 * Checkout coupon form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-coupon.php.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package DsHomeLine
 * @version 3.4.4
 */

defined( 'ABSPATH' ) || exit;

if ( ! wc_coupons_enabled() ) { // @codingStandardsIgnoreLine.
    return;
}
?>
<style>
.woocommerce-form-coupon {
    display: block!important;
}
.cart-tbl__row.coupon-wrap {
    display: inline-flex;
    flex-direction: column-reverse;
}
</style>
<div class="cart-tbl__row coupon-wrap">
    <form class="checkout_coupon woocommerce-form-coupon" method="post">
    	<div class="code">
    		<input type="text" name="coupon_code" class="code__field" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" id="coupon_code" value="" />
    		<button type="submit" class="code__submit" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_html_e( 'Apply coupon', 'woocommerce' ); ?></button>
    	</div>
    </form>
</div>
