<?php
/**
 * Checkout Payment Section
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.3
 */

defined( 'ABSPATH' ) || exit;

if ( ! is_ajax() ) {
	do_action( 'woocommerce_review_order_before_payment' );
}
?>
<?php if ( ! empty( $available_gateways ) ) { ?>
<div class="form__items">
	<div class="form__item form__item-label">
		<label for="payment_method">Способ оплаты:</label>
	</div>
	<div class="form__item" data-select-wrap>
		<div class="select">
			<select id="payment_method" name="payment_method">
				<?php 
				$firstGateway = null;
				foreach ($available_gateways as $gateway ) { 
				    if (is_null($firstGateway)) {
				        $firstGateway = $gateway;
				    }
				?>
				<option value="<?php echo $gateway->id; ?>"><?php echo $gateway->title; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="select-clone">
			<div class="select-clone__title" data-select="1">
				<p><?php echo $firstGateway->title; ?></p>
				<div class="select-clone__icon">
					<svg class="arrow-icon">
						<use class="arrow-icon__part is--dark" xlink:href="<?php echo get_template_directory_uri();?>/assets/front-end/app/img/icons/sprite.svg#right-arrow"></use>
					</svg>
				</div>
			</div>
			<ul class="select-clone__options" data-option="1">
				<?php foreach ($available_gateways as $gateway ) { ?>
				<li class="select-clone__option" value="<?php echo $gateway->id; ?>"><?php echo $gateway->title; ?></li>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>
<?php } ?>