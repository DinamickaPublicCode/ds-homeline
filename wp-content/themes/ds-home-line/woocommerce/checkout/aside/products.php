<?php
/**
 * Checkout aside-products block
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/aside.php.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package DsHomeLine
 * @version 1.0.0
 */
defined('ABSPATH') || exit();
?>
<?php
foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
    $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
    if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key)) {
?>
    <div class="cart-tbl__row">
    	<div class="cart-tbl__img">
    		<img src="<?php echo wp_get_attachment_url( $_product->get_image_id() ); ?>" />
    	</div>
    	<div class="cart-tbl__info">
    		<div class="cart-tbl__text">
    			<p class="cart-tbl__name"><strong><?php echo $cart_item['data']->get_title(); ?></strong></p>
            </div>
    		<div class="cart-tbl__descr">
            <?php
            $variation_attributes = $_product->get_variation_attributes();
            foreach ($variation_attributes as $attribute_taxonomy => $term_slug) {
                $taxonomy = str_replace('attribute_', '', $attribute_taxonomy);
                $attribute_value = get_term_by('slug', $term_slug, $taxonomy)->name;
                echo sprintf('<p>%s</p>', $attribute_value);
            }
            ?>	
            </div>
    		<div class="cart-tbl__bottom">
    			<div class="counter"><span><?php echo $cart_item['quantity']; ?></span> шт.</div>
    			<p><span><?php echo $_product->get_price(); ?></span> грн</p>
    		</div>
    		
    	</div>
    </div>
<?php
    }
}
?>