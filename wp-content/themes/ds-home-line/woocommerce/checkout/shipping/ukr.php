<?php
/**
 * Checkout Ukr Poshta block
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/aside.php.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package DsHomeLine
 * @version 1.0.0
 */
defined('ABSPATH') || exit();

$customer = wp_get_current_user();
?>
<div class="form__step-contains form__step--secondary" data-tab-content="2">
	<div class="form form--secondary">
		
				<!-- Delivery method -->
		<?php dhl_cart_totals_shipping_html(); ?>

		<div class="form__items">
			<div class="form__item form__item-label">
				<label for="first_name">Ваше имя:</label>
			</div>
			<div class="form__item">
				<input type="text" placeholder="Имя" id="first_name" name="shipping_first_name" value="<?php echo $customer->first_name; ?>">
			</div>
		</div>

		<div class="form__items">
			<div class="form__item form__item-label">
				<label for="last_name">Ваша Фамилия:</label>
			</div>
			<div class="form__item">
				<input type="text" placeholder="Фамилия" id="last_name" name="shipping_last_name" value="<?php echo $customer->last_name; ?>">
			</div>
		</div>

		<div class="form__items">
			<div class="form__item form__item-label">
				<label for="phone">Номер телефона:</label>
			</div>
			<div class="form__item">
				<input type="tel" id="phone" placeholder="+380-__-___-__-__" data-phone name="billing_phone" value="<?php echo get_user_meta( $customer->ID, 'billing_phone', true ); ?>">
			</div>
		</div>

		<div class="form__items">
			<div class="form__item form__item-label">
				<label for="shipping_city">Город:</label>
			</div>
			<div class="form__item">
				<input type="text" id="shipping_city" placeholder="Киев" name="shipping_city">
			</div>
		</div>

		<div class="form__items">
			<div class="form__item form__item-label">
				<label for="shipping_address_1">Улица, дом, квартира:</label>
			</div>

			<div class="form__item">
				<input type="text" placeholder="Улица, дом, квартира" id="shipping_address_1" name="shipping_address_1">
			</div>
		</div>

		<!-- Payment method -->
		<?php dhl_woocommerce_checkout_payment(); ?>

		<ul class="form__btns">
			<li class="form__btn">
				<input data-type="ukr" type="submit" class="btn" value="Завершить" />
			</li>
		</ul>
	</div>
</div>