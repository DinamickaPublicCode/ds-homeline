<?php
/**
 * Checkout delivery form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-deliery.php.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package DsHomeLine
 * @version 1.0.0
 */
defined('ABSPATH') || exit();

?>
<form id="chekout-form" class="form-steps">

	<fieldset class="form__step is--active" data-tabs>

		<div class="form__step-title">
			<h3>Оформление доставки</h3>
		</div>

		<ul class="form__step-items">
			<li class="form__step-item">
				<a href="javascript:void(0);" class="form__step-link is--active" data-tab="1">Новая почта</a>
			</li>
			<li class="form__step-item">
				<a href="javascript:void(0);" class="form__step-link" data-tab="2">Укрпочта</a>
			</li>
		</ul>

		<?php wc_get_template('checkout/shipping/nova.php'); ?>
		
		<?php wc_get_template('checkout/shipping/ukr.php'); ?>
		
	</fieldset>
	
</form>