<?php
/**
 * Single Product Rating
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/rating.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $product;

?>
<div class="rating">
	<a href="#" class="rating__link">6 отзывов</a>
	<div class="rating__stars">
		<input type="radio" value="1" name="star" id="star1">
		<input type="radio" value="2" name="star" id="star2">
		<input type="radio" value="3" name="star" id="star3">
		<input type="radio" value="4" name="star" id="star4">
		<input type="radio" value="5" name="star" id="star5">

		<label aria-label="1 star" class="rating__star" for="star1">
			<svg class="star-icon">
				<use class="star-icon__part" xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#star"></use>
			</svg>
		</label> 
		<label aria-label="2 stars" class="rating__star" for="star2">
			<svg class="star-icon">
				<use class="star-icon__part" xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#star"></use>
			</svg>
		</label>
		<label aria-label="3 stars" class="rating__star" for="star3">
			<svg class="star-icon">
				<use class="star-icon__part" xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#star"></use>
			</svg>
		</label>
		<label aria-label="4 stars" class="rating__star" for="star4">
			<svg class="star-icon">
				<use class="star-icon__part" xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#star"></use>
				</svg>
		</label>
		<label aria-label="5 stars" class="rating__star" for="star5">
			<svg class="star-icon">
				<use class="star-icon__part" xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#star"></use>
			</svg>
		</label>
	</div>
</div>

<?php 
/*if ( ! wc_review_ratings_enabled() ) {
	return;
}

$rating_count = $product->get_rating_count();
$review_count = $product->get_review_count();
$average      = $product->get_average_rating();

if ( $rating_count > 0 ) : ?>

	<div class="woocommerce-product-rating">
		<?php echo wc_get_rating_html( $average, $rating_count ); // WPCS: XSS ok. ?>
		<?php if ( comments_open() ) : ?>
			<?php //phpcs:disable ?>
			<a href="#reviews" class="woocommerce-review-link" rel="nofollow">(<?php printf( _n( '%s customer review', '%s customer reviews', $review_count, 'woocommerce' ), '<span class="count">' . esc_html( $review_count ) . '</span>' ); ?>)</a>
			<?php // phpcs:enable ?>
		<?php endif ?>
	</div>

<?php endif; */?>
