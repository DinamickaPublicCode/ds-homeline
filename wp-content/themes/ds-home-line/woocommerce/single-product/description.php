<?php
/**
 * Custom single-product description
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package DsHomeLine
 * @version 1.0
 * 
 */

defined( 'ABSPATH' ) || exit;
global $post, $product;

$short_description = apply_filters( 'woocommerce_short_description', $post->post_excerpt );

$sizeTable = carbon_get_theme_option('size_table');
?>

<div class="category category--secondary" data-tabs>
	<div class="category__dropdown">
				<span class="category__dropdown-text">
						Описание					</span>
				<span class="category__dropdown-icon">
					<svg class="arrow-icon">
						<use class="arrow-icon__part is--dark" xlink:href="<?php echo get_template_directory_uri(); ?>/assets/front-end/app/img/icons/sprite.svg#right-arrow"></use>
					</svg>
				</span>
			</div>
	<ul class="category__list">
		<li class="category__item">
			<a href="#"class="category__link is--active" data-tab="1">Описание</a>
		</li>
		<li class="category__item">
			<a href="#" class="category__link" data-tab="2">Детали</a>
		</li>
		<li class="category__item">
			<a href="#" class="category__link" data-tab="3">Размеры</a>
		</li>
		<li class="category__item">
			<a href="#" class="category__link" data-tab="4">Отзывы</a>
		</li>
	</ul>

	<div class="category__info full--content is--active" data-tab-content="1">
		<?php the_content(); ?>
	</div>

	<div class="category__info full--content" id="variation_description" data-tab-content="2">
		<?php echo $short_description; ?>
	</div>

	<div class="category__info full--content" data-tab-content="3">
		<?php if (!empty($sizeTable)) { ?>
		<div class="tbl-wrap is--desktop">
			<div class="tbl">
			
				<div class="tbl__row tbl--head">
					<div class="tbl__items">
						<div class="tbl__item"></div>
						<div class="tbl__item">
							<p><strong>Пододеяльник</strong></p>
						</div>
						<div class="tbl__item">
							<p><strong>Натяжная простыня</strong></p>
						</div>
						<div class="tbl__item">
							<p><strong>Простыня</strong></p>
						</div>
						<div class="tbl__item">
							<p><strong>Наволочки</strong></p>
						</div>
					</div>
				</div>
				
				<?php foreach ($sizeTable as $size) { ?>
				<div class="tbl__row tbl--body">
					<div class="tbl__items">
						<div class="tbl__item">
							<p><?php echo $size['size']; ?></p>
						</div>
						<div class="tbl__item">
							<p><?php echo $size['duvet_cover']; ?></p>
						</div>
						<div class="tbl__item">
							<p><?php echo $size['tension_sheet']; ?></p>
						</div>
						<div class="tbl__item">
							<p><?php echo $size['sheet']; ?></p>
						</div>
						<div class="tbl__item">
							<p><?php echo $size['pillowcase']; ?></p>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
		<?php } ?>
	</div>

	<div class="category__info full--content" data-tab-content="4">
		<p>Comming soon...</p>
	</div>
</div>
<script>
    function update_product_description(){
        var id = $('.variation_id').val();
        var product_variation = $("#single-variation-form").data('product_variations').filter(product_var => product_var.variation_id == id )[0];
        $('#variation_description').html(product_variation.variation_description);
    }

    jQuery( document ).ready( function () {
        update_product_description();
    } );

    jQuery( document ).on('click', "#single-variation-form input[type=radio]", function () {
        update_product_description();
    } );
</script>