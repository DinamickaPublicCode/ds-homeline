<?php
/**
 * Single Product Image
 * 
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package DsHomeLine
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
};

global $product;

$gallery = array();
$variationsGalleryStatus = $product->get_meta('dhl_variations_slider_status');

if ($variationsGalleryStatus) {
    $variations = $product->get_available_variations();
    if (!empty($variations)) {
        $gallery = getProductVariationGalleryUrls($variations);
    }
    
} else {
    $attachment_ids = $product->get_gallery_image_ids();
    if (!empty($attachment_ids)) {
        $gallery = getProductSimpleGalleryUrls($attachment_ids);
    };
}
?>
<?php if (!empty($gallery)) { ?>
    <div class="detail-card">
    	<div class="detail-card__product slick-carousel">
    		<?php foreach ($gallery as $url) { ?>
    			<div class="detail-card__img">
        			<?php if ($variationsGalleryStatus) { ?>
        			<a href="<?php echo $url['url']; ?>" class="fancybox" data-fancybox="images">
        				<img src="<?php echo $url['url']; ?>">
        			</a>
        			<?php } else { ?>
        			<a href="<?php echo $url; ?>" class="fancybox" data-fancybox="images">
        				<img src="<?php echo $url; ?>">
        			</a>
        			<?php }; ?>
    			</div>
    		<?php }; ?>
    	</div>
    
    	<div class="detail-card__dots" id="dots"></div>
    
    	<div class="detail-card__nav slick-carousel" data-slider-type="<?php echo $variationsGalleryStatus ? 'variation' : 'simple'; ?>">
    		<?php foreach ($gallery as $key => $url) { ?>
        		<?php if ($variationsGalleryStatus) { ?>
        		<div class="detail-card__item" data-variation-slide="<?php echo $key;?>">
            		<div class="detail-card__img">
            			<img class="object-fit is--cover" data-object-fit="contain" src="<?php echo $url['url']; ?>" >
            		</div>
        		</div>
        		<?php } else { ?>
        		<div class="detail-card__item">
            		<div class="detail-card__img">
            			<img class="object-fit is--cover" data-object-fit="contain" src="<?php echo $url; ?>" >
            		</div>
        		</div>
        		<?php }; ?>
    		<?php }; ?>
    	</div>
    </div>
<?php }; ?>