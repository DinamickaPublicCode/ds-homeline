<?php
/**
 * Variable product add to cart
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.5
 * 
 * @var array $attributes
 * @var array $available_variations
 */

defined( 'ABSPATH' ) || exit;

global $product;

if (empty($available_variations)) {
    $available_variations = $product->get_available_variations();
}

$attribute_keys  = array_keys( $attributes );
$variations_json = wp_json_encode( $available_variations );
$variations_attr = function_exists( 'wc_esc_json' ) ? wc_esc_json( $variations_json ) : _wp_specialchars( $variations_json, ENT_QUOTES, 'UTF-8', true );

do_action( 'woocommerce_before_add_to_cart_form' );
?>


<form id="single-variation-form" class="variations_form cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product->get_id() ); ?>"  data-product_variations="<?php echo $variations_attr; // WPCS: XSS ok. ?>">
	
	<?php do_action( 'woocommerce_before_variations_form' ); ?>

	<?php if ( empty( $available_variations ) && false !== $available_variations ) { ?>
		<p class="stock out-of-stock"><?php esc_html_e( 'This product is currently out of stock and unavailable.', 'woocommerce' ); ?></p>
	<?php } else { ?>
		<?php 
    	foreach ( $attributes as $attribute_name => $options ) { 
            echo dhl_get_variations_list(array(
               'options'   => $options,
               'attribute' => $attribute_name,
               'product'   => $product,
            ));
        } 
        ?>
    <?php }; ?>
    
    <?php
    	/**
    	 * Hook: woocommerce_before_single_variation.
    	 */
    	do_action( 'woocommerce_before_single_variation' );
    
    	/**
    	 * Hook: woocommerce_single_variation. Used to output the cart button and placeholder for variation data.
    	 *
    	 * @since 2.4.0
    	 * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.
    	 */
    	do_action( 'woocommerce_single_variation' );
    
    	/**
    	 * Hook: woocommerce_after_single_variation.
    	 */
    	do_action( 'woocommerce_after_single_variation' );
	?>
	
	<?php do_action( 'woocommerce_after_variations_form' );?>
</form>

<?php
do_action( 'woocommerce_after_add_to_cart_form' );
