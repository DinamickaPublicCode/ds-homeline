<?php
/**
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
?>
<div id="variation-price"></div>

<div class="info-card__row">
	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

	<?php
	do_action( 'woocommerce_before_add_to_cart_quantity' );

// 	woocommerce_quantity_input( array(
// 		'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
// 		'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
// 		'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
// 	) );

	do_action( 'woocommerce_after_add_to_cart_quantity' );
	?>

	<ul class="info-card__list" id="list-btn" data-list-btn="list-btn">
		<li class="info-card__item">
			<button type="submit" class="btn">В корзину</button>
		</li>
		<li class="info-card__item">
			<a href="#" class="btn btn--fourth" data-get-popup="form-order">Купить в один клик</a>
		</li>
	</ul>
	
	<div class="info-card__text">
		<p>Доставка до трех дней. <a href="<?php echo carbon_get_theme_option('dostavka_oplata_page_link'); ?>">Подробнее...</a></p>
	</div>

	<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

	<input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="quantity" value="1" />
	<input type="hidden" name="variation_id" class="variation_id" value="0" />
	
</div>
